import sys
from ast import literal_eval as make_tuple


def parse_certificate(cert_file):
    g2b = {}
    b2g = {}
    clauses = []
    cas_var = None
    with open(cert_file, 'r') as stream:
        section = 0
        for line in stream:
            line = line.strip()
            if line == "V2G":
                section = 1
            elif line == "Predicates":
                section = 2
            elif line == "Clauses":
                section = 3
            elif section == 1:
                # process the graph-boolean mapping
                dimacs_var, rest = int(line.split()[0]), " ".join(line.split()[1:])
                # parses the remainder of the string into a python tuple
                # for the two case studies, since we only have one graph variable, we take the simpler approach
                # of ignoring the graph id 'x'
                tup = make_tuple(rest)
                graph_component = tup[1]
                g2b[graph_component] = dimacs_var
                b2g[dimacs_var] = graph_component
            elif section == 2:
                # get the dimacs vars for the cas predicates
                # again, since we only have one cas predicate in the case study, just grab the single dimacs var.
                cas_var = int(line.split()[0])
            elif section == 3:
                # get the extra clauses learned from the run of the solver
                # will append these to the final dimacs file to actually get an unsat result
                clauses.append([int(i) for i in line.strip().split()])
    return g2b, b2g, cas_var, clauses

def hamming_distance(s1, s2):
    """
    http://en.wikipedia.org/wiki/Hamming_distance

    Return the Hamming distance between equal-length sequences
    """
    if len(s1) != len(s2):
        raise ValueError("Undefined for sequences of unequal length")
    return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))


def create_cube(dims):
    vertices = range(2 ** dims)
    edges = []
    # an edge exists between 2 vertices in the hypercube if their bit representation hamming distance is one.
    binary_format_string = "{:0" + str(dims) + "b}"
    for i in vertices:
        for j in vertices:
            b_i = binary_format_string.format(i)
            b_j = binary_format_string.format(j)
            if i < j and hamming_distance(b_i, b_j) == 1:
                edges.append((i, j))
    return vertices, edges


def check_clause(c, b2g, cas_var, cube_vertices, cube_edges):
    # ensure the cas predicate abstraction is in the clause
    assert cas_var in c

    #ensure the remaining variables correspond to edges
    graph_components = [b2g[i] for i in c if i != cas_var]

    assert set(graph_components).issubset(cube_edges)

    complemented_edges = list(set(cube_edges).difference(graph_components))

    # check if the complemented_edges form a hamiltonian cycle
    visited = []
    (first_vertex, next) = complemented_edges[0]
    complemented_edges.pop(0)
    visited.append(first_vertex)
    while True:
        next_edges = [(v1, v2) for (v1, v2) in complemented_edges if v1 == next or v2 == next]
        if len(next_edges) > 1:
            sys.exit("Bug in cycle detection")
        if len(next_edges) != 1:
            assert len(complemented_edges) == 0
            if next == first_vertex and set(visited) == set(cube_vertices):
                break
            else:
                sys.exit("Bug in cycle detection")
        visited.append(next)
        (v1, v2) = next_edges[0]
        complemented_edges.remove((v1, v2))
        if v1 == next:
            next = v2
        else:
            next = v1


def main(dims, cert_file, cnf_file):
    # create the hypercube, which is effectively a tuple containing a list of vertices and edges, represented as ints
    cube_vertices, cube_edges = create_cube(dims)

    # maps each graph component to its corresponding dimacs variable (g2b),
    # as well as the inverse map (b2g), according to the given certificate.
    # obtains the dimacs for the cas_var, as well as the learnt clauses
    g2b, b2g, cas_var, learnt_clauses = parse_certificate(cert_file)

    # Generate clauses -- throughout, we use the terms 'on' and 'off' to mean that the graph component
    #                     is in the model or not, respectively.
    clauses = []

    # assert the edge-implies-vertices constraints
    for e in cube_edges:
        # either the edge e is off, or both of its vertices v1, v2 are on
        # (!e or v1) and (!e or v2)
        (v1, v2) = e
        clauses.append([-g2b[e], g2b[v1]])
        clauses.append([-g2b[e], g2b[v2]])

    # assert edge-on constraints -- if a vertex v is on, one of its incident edges must be on
    # (!v or some incident edge)
    for v in cube_vertices:
        incident_edges = [(v1, v2) for (v1, v2) in cube_edges if v1 == v or v2 == v]
        clauses.append([-g2b[v]] + [g2b[e] for e in incident_edges])

    # assert matching constraints
    for v in cube_vertices:
        # get all edges incident to v
        incident_edges = [(v1, v2) for (v1, v2) in cube_edges if v1 == v or v2 == v]
        # for any pair of incident edges, they cannot be both on
        # (!e1 or !e2)
        for i in range(len(incident_edges)):
            for j in range(i + 1, len(incident_edges)):
                e1 = incident_edges[i]
                e2 = incident_edges[j]
                clauses.append([-g2b[e1], -g2b[e2]])

    # assert maximal constraints.
    # For any edge e=(v1, v2) in the graph, either v1 is on or v2 is on: (v1 or v2)
    # Used in conjunction with edge-on constraints to ensure that the matching is maximal.
    for e in cube_edges:
        (v1, v2) = e
        clauses.append([g2b[v1], g2b[v2]])

    # assert imperfect matching constraints. v0 is always the excluded vertex.
    v = cube_vertices[0]
    incident_edges = [(v1, v2) for (v1, v2) in cube_edges if v1 == v or v2 == v]
    for e in incident_edges:
        # no edge incident to v0 can be in the matching
        # (!e)
        clauses.append([-g2b[e]])

    # append the negation of the cas_var literal, as in the paper (we want to ensure the predicate does not hold)
    clauses.append([-cas_var])

    # append learnt clauses from certificate
    for c in learnt_clauses:
        # ensure that the clause corresponds to equation 7 in the paper
        check_clause(c, b2g, cas_var, cube_vertices, cube_edges)
        clauses.append(c)

    # write header to cnf file
    out = open(cnf_file, 'w')
    out.write("p cnf " + str(len(cube_vertices) + len(cube_edges)) + " " + str(len(clauses)) + "\n")

    # write clauses
    for c in clauses:
        out.write(" ".join([str(i) for i in c]) + " 0\n")

    out.close()


if __name__ == '__main__':
    if len(sys.argv) != 4:
        sys.exit("Usage: python3 hamilton_clean #dims certificate_file out_cnf_file")
    main(int(sys.argv[1]), sys.argv[2], sys.argv[3])
