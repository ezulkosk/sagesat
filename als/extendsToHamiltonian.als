open cubes

/* no edges overlap and x is within G */
pred matching[x: Graph, G : Graph] {
  x.edges in G.edges
  all disj a,b: x.edges | no (a.ends & b.ends)
}

/* The matching is not perfect */
/* We specifically choose n0 as in the SAT encoding, but maybe not necessary in alloy. */


/* The matching is maximal */
pred maximal[x: Graph, G : Graph] {
  all e : G.edges | some e.ends & x.nodes
}

/* Node n has degree 2 in E */
pred deg2[n : Node, E : set(Edge)]{
  some disj e1, e2 : E | n in e1.ends && n in e2.ends
  no disj e1, e2, e3 : E | n in e1.ends && n in e2.ends && n in e3.ends
}

pred node_part[U : set Node, V : set Node]{
  no U & V
  all n : Node | n in U || n in V
}

pred span[u : Node, v : Node, C : set Edge] {
  all V, W : set Node when (node_part[V,W] && u in V && v in W) |
    (some e : Edge, x, y : Node | x in e.ends && y in e.ends && x in V && y in W && e in C)
}


one sig x extends Graph {}
{
  //no extra nodes outside the matching
  nodes in edges.ends 
}

