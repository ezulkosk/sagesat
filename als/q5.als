open cubes

one sig n0 extends Node { }
one sig n1 extends Node { }
one sig n2 extends Node { }
one sig n3 extends Node { }
one sig n4 extends Node { }
one sig n5 extends Node { }
one sig n6 extends Node { }
one sig n7 extends Node { }
one sig n8 extends Node { }
one sig n9 extends Node { }
one sig n10 extends Node { }
one sig n11 extends Node { }
one sig n12 extends Node { }
one sig n13 extends Node { }
one sig n14 extends Node { }
one sig n15 extends Node { }
one sig n16 extends Node { }
one sig n17 extends Node { }
one sig n18 extends Node { }
one sig n19 extends Node { }
one sig n20 extends Node { }
one sig n21 extends Node { }
one sig n22 extends Node { }
one sig n23 extends Node { }
one sig n24 extends Node { }
one sig n25 extends Node { }
one sig n26 extends Node { }
one sig n27 extends Node { }
one sig n28 extends Node { }
one sig n29 extends Node { }
one sig n30 extends Node { }
one sig n31 extends Node { }

one sig e0_1 extends Edge { } {
  nodes = n0 + n1 }
one sig e0_2 extends Edge { } {
  nodes = n0 + n2 }
one sig e0_4 extends Edge { } {
  nodes = n0 + n4 }
one sig e0_8 extends Edge { } {
  nodes = n0 + n8 }
one sig e0_16 extends Edge { } {
  nodes = n0 + n16 }
one sig e1_3 extends Edge { } {
  nodes = n1 + n3 }
one sig e1_5 extends Edge { } {
  nodes = n1 + n5 }
one sig e1_9 extends Edge { } {
  nodes = n1 + n9 }
one sig e1_17 extends Edge { } {
  nodes = n1 + n17 }
one sig e2_3 extends Edge { } {
  nodes = n2 + n3 }
one sig e2_6 extends Edge { } {
  nodes = n2 + n6 }
one sig e2_10 extends Edge { } {
  nodes = n2 + n10 }
one sig e2_18 extends Edge { } {
  nodes = n2 + n18 }
one sig e3_7 extends Edge { } {
  nodes = n3 + n7 }
one sig e3_11 extends Edge { } {
  nodes = n3 + n11 }
one sig e3_19 extends Edge { } {
  nodes = n3 + n19 }
one sig e4_5 extends Edge { } {
  nodes = n4 + n5 }
one sig e4_6 extends Edge { } {
  nodes = n4 + n6 }
one sig e4_12 extends Edge { } {
  nodes = n4 + n12 }
one sig e4_20 extends Edge { } {
  nodes = n4 + n20 }
one sig e5_7 extends Edge { } {
  nodes = n5 + n7 }
one sig e5_13 extends Edge { } {
  nodes = n5 + n13 }
one sig e5_21 extends Edge { } {
  nodes = n5 + n21 }
one sig e6_7 extends Edge { } {
  nodes = n6 + n7 }
one sig e6_14 extends Edge { } {
  nodes = n6 + n14 }
one sig e6_22 extends Edge { } {
  nodes = n6 + n22 }
one sig e7_15 extends Edge { } {
  nodes = n7 + n15 }
one sig e7_23 extends Edge { } {
  nodes = n7 + n23 }
one sig e8_9 extends Edge { } {
  nodes = n8 + n9 }
one sig e8_10 extends Edge { } {
  nodes = n8 + n10 }
one sig e8_12 extends Edge { } {
  nodes = n8 + n12 }
one sig e8_24 extends Edge { } {
  nodes = n8 + n24 }
one sig e9_11 extends Edge { } {
  nodes = n9 + n11 }
one sig e9_13 extends Edge { } {
  nodes = n9 + n13 }
one sig e9_25 extends Edge { } {
  nodes = n9 + n25 }
one sig e10_11 extends Edge { } {
  nodes = n10 + n11 }
one sig e10_14 extends Edge { } {
  nodes = n10 + n14 }
one sig e10_26 extends Edge { } {
  nodes = n10 + n26 }
one sig e11_15 extends Edge { } {
  nodes = n11 + n15 }
one sig e11_27 extends Edge { } {
  nodes = n11 + n27 }
one sig e12_13 extends Edge { } {
  nodes = n12 + n13 }
one sig e12_14 extends Edge { } {
  nodes = n12 + n14 }
one sig e12_28 extends Edge { } {
  nodes = n12 + n28 }
one sig e13_15 extends Edge { } {
  nodes = n13 + n15 }
one sig e13_29 extends Edge { } {
  nodes = n13 + n29 }
one sig e14_15 extends Edge { } {
  nodes = n14 + n15 }
one sig e14_30 extends Edge { } {
  nodes = n14 + n30 }
one sig e15_31 extends Edge { } {
  nodes = n15 + n31 }
one sig e16_17 extends Edge { } {
  nodes = n16 + n17 }
one sig e16_18 extends Edge { } {
  nodes = n16 + n18 }
one sig e16_20 extends Edge { } {
  nodes = n16 + n20 }
one sig e16_24 extends Edge { } {
  nodes = n16 + n24 }
one sig e17_19 extends Edge { } {
  nodes = n17 + n19 }
one sig e17_21 extends Edge { } {
  nodes = n17 + n21 }
one sig e17_25 extends Edge { } {
  nodes = n17 + n25 }
one sig e18_19 extends Edge { } {
  nodes = n18 + n19 }
one sig e18_22 extends Edge { } {
  nodes = n18 + n22 }
one sig e18_26 extends Edge { } {
  nodes = n18 + n26 }
one sig e19_23 extends Edge { } {
  nodes = n19 + n23 }
one sig e19_27 extends Edge { } {
  nodes = n19 + n27 }
one sig e20_21 extends Edge { } {
  nodes = n20 + n21 }
one sig e20_22 extends Edge { } {
  nodes = n20 + n22 }
one sig e20_28 extends Edge { } {
  nodes = n20 + n28 }
one sig e21_23 extends Edge { } {
  nodes = n21 + n23 }
one sig e21_29 extends Edge { } {
  nodes = n21 + n29 }
one sig e22_23 extends Edge { } {
  nodes = n22 + n23 }
one sig e22_30 extends Edge { } {
  nodes = n22 + n30 }
one sig e23_31 extends Edge { } {
  nodes = n23 + n31 }
one sig e24_25 extends Edge { } {
  nodes = n24 + n25 }
one sig e24_26 extends Edge { } {
  nodes = n24 + n26 }
one sig e24_28 extends Edge { } {
  nodes = n24 + n28 }
one sig e25_27 extends Edge { } {
  nodes = n25 + n27 }
one sig e25_29 extends Edge { } {
  nodes = n25 + n29 }
one sig e26_27 extends Edge { } {
  nodes = n26 + n27 }
one sig e26_30 extends Edge { } {
  nodes = n26 + n30 }
one sig e27_31 extends Edge { } {
  nodes = n27 + n31 }
one sig e28_29 extends Edge { } {
  nodes = n28 + n29 }
one sig e28_30 extends Edge { } {
  nodes = n28 + n30 }
one sig e29_31 extends Edge { } {
  nodes = n29 + n31 }
one sig e30_31 extends Edge { } {
  nodes = n30 + n31 }

one sig Q5 extends Graph { } {
  nodes = n0 + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10 + n11 + n12 + n13 + n14 + n15 + n16 + n17 + n18 + n19 + n20 + n21 + n22 + n23 + n24 + n25 + n26 + n27 + n28 + n29 + n30 + n31
  edges = e0_1 + e0_2 + e0_4 + e0_8 + e0_16 + e1_3 + e1_5 + e1_9 + e1_17 + e2_3 + e2_6 + e2_10 + e2_18 + e3_7 + e3_11 + e3_19 + e4_5 + e4_6 + e4_12 + e4_20 + e5_7 + e5_13 + e5_21 + e6_7 + e6_14 + e6_22 + e7_15 + e7_23 + e8_9 + e8_10 + e8_12 + e8_24 + e9_11 + e9_13 + e9_25 + e10_11 + e10_14 + e10_26 + e11_15 + e11_27 + e12_13 + e12_14 + e12_28 + e13_15 + e13_29 + e14_15 + e14_30 + e15_31 + e16_17 + e16_18 + e16_20 + e16_24 + e17_19 + e17_21 + e17_25 + e18_19 + e18_22 + e18_26 + e19_23 + e19_27 + e20_21 + e20_22 + e20_28 + e21_23 + e21_29 + e22_23 + e22_30 + e23_31 + e24_25 + e24_26 + e24_28 + e25_27 + e25_29 + e26_27 + e26_30 + e27_31 + e28_29 + e28_30 + e29_31 + e30_31
}

pred imperfect[x: Graph, G : Graph] {
  no (e0_1 + e0_2 + e0_4 + e0_8 + e0_16) & x.edges
}

