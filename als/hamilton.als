
sig Vertex {}

sig Edge
{
	vertices : set Vertex // clauses -- 
}
{
	#vertices = 2
}

fact { 
	no disj e1, e2 : Edge | e1.vertices = e2.vertices
   all v : Vertex | # vertices.v = 3
}

run {} for exactly 8 Vertex, exactly 12 Edge
