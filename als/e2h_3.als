open extendsToHamiltonian
open q3

//all edges must belong to some graph
fact{ 
  all e : Edge | e in Q3.edges || e in x.edges 
}


pred extends_to_hamiltonian[x: Graph, G : Graph] {
  some C : set(Edge) when (matching[x, G] && imperfect[x, G] && maximal[x, G]) {
    x.edges in C
    all u : Node | deg2[u, C]
    all u, v : Node | span[u, v, C]
    //all u,v : Node | deg2[u, C] && span(u, v, C) && conn(u,v, W)
  }
}

check e2h {
  extends_to_hamiltonian[x, Q3]
} for exactly 2 Graph, 8 Node, 12 Edge
