open cubes 

one sig n0 extends Node { }
one sig n1 extends Node { }
one sig n2 extends Node { }
one sig n3 extends Node { }
one sig n4 extends Node { }
one sig n5 extends Node { }
one sig n6 extends Node { }
one sig n7 extends Node { }

one sig e0_1 extends Edge { } {
  ends = n0 + n1 }
one sig e0_2 extends Edge { } {
  ends = n0 + n2 }
one sig e0_4 extends Edge { } {
  ends = n0 + n4 }
one sig e1_3 extends Edge { } {
  ends = n1 + n3 }
one sig e1_5 extends Edge { } {
  ends = n1 + n5 }
one sig e2_3 extends Edge { } {
  ends = n2 + n3 }
one sig e2_6 extends Edge { } {
  ends = n2 + n6 }
one sig e3_7 extends Edge { } {
  ends = n3 + n7 }
one sig e4_5 extends Edge { } {
  ends = n4 + n5 }
one sig e4_6 extends Edge { } {
  ends = n4 + n6 }
one sig e5_7 extends Edge { } {
  ends = n5 + n7 }
one sig e6_7 extends Edge { } {
  ends = n6 + n7 }

one sig Q3 extends Graph { } {
  nodes = n0 + n1 + n2 + n3 + n4 + n5 + n6 + n7
  edges = e0_1 + e0_2 + e0_4 + e1_3 + e1_5 + e2_3 + e2_6 + e3_7 + e4_5 + e4_6 + e5_7 + e6_7
}

pred imperfect[x: Graph, G : Graph] {
  no (e0_1 + e0_2 + e0_4) & x.edges
}

pred antipodal[x: Graph] {
	one (e0_1 + e6_7) & x.edges
	one (e0_2 + e5_7) & x.edges
	one (e0_4 + e3_7) & x.edges
	one (e1_3 + e4_6) & x.edges
	one (e1_5 + e2_6) & x.edges
	one (e2_3 + e4_5) & x.edges
}

pred nonsimple[x: Graph] {
(e0_1 not in x.edges && e1_3 in x.edges && e2_3 not in x.edges && e0_2 in x.edges) || (e0_1 in x.edges && e1_3 not in x.edges && e2_3 in x.edges && e0_2 not in x.edges)||
(e4_5 not in x.edges && e5_7 in x.edges && e6_7 not in x.edges && e4_6 in x.edges) || (e4_5 in x.edges && e5_7 not in x.edges && e6_7 in x.edges && e4_6 not in x.edges)||
(e0_1 not in x.edges && e1_5 in x.edges && e4_5 not in x.edges && e0_4 in x.edges) || (e0_1 in x.edges && e1_5 not in x.edges && e4_5 in x.edges && e0_4 not in x.edges)||
(e2_3 not in x.edges && e3_7 in x.edges && e6_7 not in x.edges && e2_6 in x.edges) || (e2_3 in x.edges && e3_7 not in x.edges && e6_7 in x.edges && e2_6 not in x.edges)||
(e0_2 not in x.edges && e2_6 in x.edges && e4_6 not in x.edges && e0_4 in x.edges) || (e0_2 in x.edges && e2_6 not in x.edges && e4_6 in x.edges && e0_4 not in x.edges)||
(e1_3 not in x.edges && e3_7 in x.edges && e5_7 not in x.edges && e1_5 in x.edges) || (e1_3 in x.edges && e3_7 not in x.edges && e5_7 in x.edges && e1_5 not in x.edges)

}

