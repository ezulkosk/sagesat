
one sig n0 extends Node { }
one sig n1 extends Node { }
one sig n2 extends Node { }
one sig n3 extends Node { }
one sig n4 extends Node { }
one sig n5 extends Node { }
one sig n6 extends Node { }
one sig n7 extends Node { }
one sig n8 extends Node { }
one sig n9 extends Node { }
one sig n10 extends Node { }
one sig n11 extends Node { }
one sig n12 extends Node { }
one sig n13 extends Node { }
one sig n14 extends Node { }
one sig n15 extends Node { }
one sig n16 extends Node { }
one sig n17 extends Node { }
one sig n18 extends Node { }
one sig n19 extends Node { }
one sig n20 extends Node { }
one sig n21 extends Node { }
one sig n22 extends Node { }
one sig n23 extends Node { }
one sig n24 extends Node { }
one sig n25 extends Node { }
one sig n26 extends Node { }
one sig n27 extends Node { }
one sig n28 extends Node { }
one sig n29 extends Node { }
one sig n30 extends Node { }
one sig n31 extends Node { }
one sig n32 extends Node { }
one sig n33 extends Node { }
one sig n34 extends Node { }
one sig n35 extends Node { }
one sig n36 extends Node { }
one sig n37 extends Node { }
one sig n38 extends Node { }
one sig n39 extends Node { }
one sig n40 extends Node { }
one sig n41 extends Node { }
one sig n42 extends Node { }
one sig n43 extends Node { }
one sig n44 extends Node { }
one sig n45 extends Node { }
one sig n46 extends Node { }
one sig n47 extends Node { }
one sig n48 extends Node { }
one sig n49 extends Node { }
one sig n50 extends Node { }
one sig n51 extends Node { }
one sig n52 extends Node { }
one sig n53 extends Node { }
one sig n54 extends Node { }
one sig n55 extends Node { }
one sig n56 extends Node { }
one sig n57 extends Node { }
one sig n58 extends Node { }
one sig n59 extends Node { }
one sig n60 extends Node { }
one sig n61 extends Node { }
one sig n62 extends Node { }
one sig n63 extends Node { }

one sig e0_1 extends Edge { } {
  ends = n0 + n1 }
one sig e0_2 extends Edge { } {
  ends = n0 + n2 }
one sig e0_4 extends Edge { } {
  ends = n0 + n4 }
one sig e0_8 extends Edge { } {
  ends = n0 + n8 }
one sig e0_16 extends Edge { } {
  ends = n0 + n16 }
one sig e0_32 extends Edge { } {
  ends = n0 + n32 }
one sig e1_3 extends Edge { } {
  ends = n1 + n3 }
one sig e1_5 extends Edge { } {
  ends = n1 + n5 }
one sig e1_9 extends Edge { } {
  ends = n1 + n9 }
one sig e1_17 extends Edge { } {
  ends = n1 + n17 }
one sig e1_33 extends Edge { } {
  ends = n1 + n33 }
one sig e2_3 extends Edge { } {
  ends = n2 + n3 }
one sig e2_6 extends Edge { } {
  ends = n2 + n6 }
one sig e2_10 extends Edge { } {
  ends = n2 + n10 }
one sig e2_18 extends Edge { } {
  ends = n2 + n18 }
one sig e2_34 extends Edge { } {
  ends = n2 + n34 }
one sig e3_7 extends Edge { } {
  ends = n3 + n7 }
one sig e3_11 extends Edge { } {
  ends = n3 + n11 }
one sig e3_19 extends Edge { } {
  ends = n3 + n19 }
one sig e3_35 extends Edge { } {
  ends = n3 + n35 }
one sig e4_5 extends Edge { } {
  ends = n4 + n5 }
one sig e4_6 extends Edge { } {
  ends = n4 + n6 }
one sig e4_12 extends Edge { } {
  ends = n4 + n12 }
one sig e4_20 extends Edge { } {
  ends = n4 + n20 }
one sig e4_36 extends Edge { } {
  ends = n4 + n36 }
one sig e5_7 extends Edge { } {
  ends = n5 + n7 }
one sig e5_13 extends Edge { } {
  ends = n5 + n13 }
one sig e5_21 extends Edge { } {
  ends = n5 + n21 }
one sig e5_37 extends Edge { } {
  ends = n5 + n37 }
one sig e6_7 extends Edge { } {
  ends = n6 + n7 }
one sig e6_14 extends Edge { } {
  ends = n6 + n14 }
one sig e6_22 extends Edge { } {
  ends = n6 + n22 }
one sig e6_38 extends Edge { } {
  ends = n6 + n38 }
one sig e7_15 extends Edge { } {
  ends = n7 + n15 }
one sig e7_23 extends Edge { } {
  ends = n7 + n23 }
one sig e7_39 extends Edge { } {
  ends = n7 + n39 }
one sig e8_9 extends Edge { } {
  ends = n8 + n9 }
one sig e8_10 extends Edge { } {
  ends = n8 + n10 }
one sig e8_12 extends Edge { } {
  ends = n8 + n12 }
one sig e8_24 extends Edge { } {
  ends = n8 + n24 }
one sig e8_40 extends Edge { } {
  ends = n8 + n40 }
one sig e9_11 extends Edge { } {
  ends = n9 + n11 }
one sig e9_13 extends Edge { } {
  ends = n9 + n13 }
one sig e9_25 extends Edge { } {
  ends = n9 + n25 }
one sig e9_41 extends Edge { } {
  ends = n9 + n41 }
one sig e10_11 extends Edge { } {
  ends = n10 + n11 }
one sig e10_14 extends Edge { } {
  ends = n10 + n14 }
one sig e10_26 extends Edge { } {
  ends = n10 + n26 }
one sig e10_42 extends Edge { } {
  ends = n10 + n42 }
one sig e11_15 extends Edge { } {
  ends = n11 + n15 }
one sig e11_27 extends Edge { } {
  ends = n11 + n27 }
one sig e11_43 extends Edge { } {
  ends = n11 + n43 }
one sig e12_13 extends Edge { } {
  ends = n12 + n13 }
one sig e12_14 extends Edge { } {
  ends = n12 + n14 }
one sig e12_28 extends Edge { } {
  ends = n12 + n28 }
one sig e12_44 extends Edge { } {
  ends = n12 + n44 }
one sig e13_15 extends Edge { } {
  ends = n13 + n15 }
one sig e13_29 extends Edge { } {
  ends = n13 + n29 }
one sig e13_45 extends Edge { } {
  ends = n13 + n45 }
one sig e14_15 extends Edge { } {
  ends = n14 + n15 }
one sig e14_30 extends Edge { } {
  ends = n14 + n30 }
one sig e14_46 extends Edge { } {
  ends = n14 + n46 }
one sig e15_31 extends Edge { } {
  ends = n15 + n31 }
one sig e15_47 extends Edge { } {
  ends = n15 + n47 }
one sig e16_17 extends Edge { } {
  ends = n16 + n17 }
one sig e16_18 extends Edge { } {
  ends = n16 + n18 }
one sig e16_20 extends Edge { } {
  ends = n16 + n20 }
one sig e16_24 extends Edge { } {
  ends = n16 + n24 }
one sig e16_48 extends Edge { } {
  ends = n16 + n48 }
one sig e17_19 extends Edge { } {
  ends = n17 + n19 }
one sig e17_21 extends Edge { } {
  ends = n17 + n21 }
one sig e17_25 extends Edge { } {
  ends = n17 + n25 }
one sig e17_49 extends Edge { } {
  ends = n17 + n49 }
one sig e18_19 extends Edge { } {
  ends = n18 + n19 }
one sig e18_22 extends Edge { } {
  ends = n18 + n22 }
one sig e18_26 extends Edge { } {
  ends = n18 + n26 }
one sig e18_50 extends Edge { } {
  ends = n18 + n50 }
one sig e19_23 extends Edge { } {
  ends = n19 + n23 }
one sig e19_27 extends Edge { } {
  ends = n19 + n27 }
one sig e19_51 extends Edge { } {
  ends = n19 + n51 }
one sig e20_21 extends Edge { } {
  ends = n20 + n21 }
one sig e20_22 extends Edge { } {
  ends = n20 + n22 }
one sig e20_28 extends Edge { } {
  ends = n20 + n28 }
one sig e20_52 extends Edge { } {
  ends = n20 + n52 }
one sig e21_23 extends Edge { } {
  ends = n21 + n23 }
one sig e21_29 extends Edge { } {
  ends = n21 + n29 }
one sig e21_53 extends Edge { } {
  ends = n21 + n53 }
one sig e22_23 extends Edge { } {
  ends = n22 + n23 }
one sig e22_30 extends Edge { } {
  ends = n22 + n30 }
one sig e22_54 extends Edge { } {
  ends = n22 + n54 }
one sig e23_31 extends Edge { } {
  ends = n23 + n31 }
one sig e23_55 extends Edge { } {
  ends = n23 + n55 }
one sig e24_25 extends Edge { } {
  ends = n24 + n25 }
one sig e24_26 extends Edge { } {
  ends = n24 + n26 }
one sig e24_28 extends Edge { } {
  ends = n24 + n28 }
one sig e24_56 extends Edge { } {
  ends = n24 + n56 }
one sig e25_27 extends Edge { } {
  ends = n25 + n27 }
one sig e25_29 extends Edge { } {
  ends = n25 + n29 }
one sig e25_57 extends Edge { } {
  ends = n25 + n57 }
one sig e26_27 extends Edge { } {
  ends = n26 + n27 }
one sig e26_30 extends Edge { } {
  ends = n26 + n30 }
one sig e26_58 extends Edge { } {
  ends = n26 + n58 }
one sig e27_31 extends Edge { } {
  ends = n27 + n31 }
one sig e27_59 extends Edge { } {
  ends = n27 + n59 }
one sig e28_29 extends Edge { } {
  ends = n28 + n29 }
one sig e28_30 extends Edge { } {
  ends = n28 + n30 }
one sig e28_60 extends Edge { } {
  ends = n28 + n60 }
one sig e29_31 extends Edge { } {
  ends = n29 + n31 }
one sig e29_61 extends Edge { } {
  ends = n29 + n61 }
one sig e30_31 extends Edge { } {
  ends = n30 + n31 }
one sig e30_62 extends Edge { } {
  ends = n30 + n62 }
one sig e31_63 extends Edge { } {
  ends = n31 + n63 }
one sig e32_33 extends Edge { } {
  ends = n32 + n33 }
one sig e32_34 extends Edge { } {
  ends = n32 + n34 }
one sig e32_36 extends Edge { } {
  ends = n32 + n36 }
one sig e32_40 extends Edge { } {
  ends = n32 + n40 }
one sig e32_48 extends Edge { } {
  ends = n32 + n48 }
one sig e33_35 extends Edge { } {
  ends = n33 + n35 }
one sig e33_37 extends Edge { } {
  ends = n33 + n37 }
one sig e33_41 extends Edge { } {
  ends = n33 + n41 }
one sig e33_49 extends Edge { } {
  ends = n33 + n49 }
one sig e34_35 extends Edge { } {
  ends = n34 + n35 }
one sig e34_38 extends Edge { } {
  ends = n34 + n38 }
one sig e34_42 extends Edge { } {
  ends = n34 + n42 }
one sig e34_50 extends Edge { } {
  ends = n34 + n50 }
one sig e35_39 extends Edge { } {
  ends = n35 + n39 }
one sig e35_43 extends Edge { } {
  ends = n35 + n43 }
one sig e35_51 extends Edge { } {
  ends = n35 + n51 }
one sig e36_37 extends Edge { } {
  ends = n36 + n37 }
one sig e36_38 extends Edge { } {
  ends = n36 + n38 }
one sig e36_44 extends Edge { } {
  ends = n36 + n44 }
one sig e36_52 extends Edge { } {
  ends = n36 + n52 }
one sig e37_39 extends Edge { } {
  ends = n37 + n39 }
one sig e37_45 extends Edge { } {
  ends = n37 + n45 }
one sig e37_53 extends Edge { } {
  ends = n37 + n53 }
one sig e38_39 extends Edge { } {
  ends = n38 + n39 }
one sig e38_46 extends Edge { } {
  ends = n38 + n46 }
one sig e38_54 extends Edge { } {
  ends = n38 + n54 }
one sig e39_47 extends Edge { } {
  ends = n39 + n47 }
one sig e39_55 extends Edge { } {
  ends = n39 + n55 }
one sig e40_41 extends Edge { } {
  ends = n40 + n41 }
one sig e40_42 extends Edge { } {
  ends = n40 + n42 }
one sig e40_44 extends Edge { } {
  ends = n40 + n44 }
one sig e40_56 extends Edge { } {
  ends = n40 + n56 }
one sig e41_43 extends Edge { } {
  ends = n41 + n43 }
one sig e41_45 extends Edge { } {
  ends = n41 + n45 }
one sig e41_57 extends Edge { } {
  ends = n41 + n57 }
one sig e42_43 extends Edge { } {
  ends = n42 + n43 }
one sig e42_46 extends Edge { } {
  ends = n42 + n46 }
one sig e42_58 extends Edge { } {
  ends = n42 + n58 }
one sig e43_47 extends Edge { } {
  ends = n43 + n47 }
one sig e43_59 extends Edge { } {
  ends = n43 + n59 }
one sig e44_45 extends Edge { } {
  ends = n44 + n45 }
one sig e44_46 extends Edge { } {
  ends = n44 + n46 }
one sig e44_60 extends Edge { } {
  ends = n44 + n60 }
one sig e45_47 extends Edge { } {
  ends = n45 + n47 }
one sig e45_61 extends Edge { } {
  ends = n45 + n61 }
one sig e46_47 extends Edge { } {
  ends = n46 + n47 }
one sig e46_62 extends Edge { } {
  ends = n46 + n62 }
one sig e47_63 extends Edge { } {
  ends = n47 + n63 }
one sig e48_49 extends Edge { } {
  ends = n48 + n49 }
one sig e48_50 extends Edge { } {
  ends = n48 + n50 }
one sig e48_52 extends Edge { } {
  ends = n48 + n52 }
one sig e48_56 extends Edge { } {
  ends = n48 + n56 }
one sig e49_51 extends Edge { } {
  ends = n49 + n51 }
one sig e49_53 extends Edge { } {
  ends = n49 + n53 }
one sig e49_57 extends Edge { } {
  ends = n49 + n57 }
one sig e50_51 extends Edge { } {
  ends = n50 + n51 }
one sig e50_54 extends Edge { } {
  ends = n50 + n54 }
one sig e50_58 extends Edge { } {
  ends = n50 + n58 }
one sig e51_55 extends Edge { } {
  ends = n51 + n55 }
one sig e51_59 extends Edge { } {
  ends = n51 + n59 }
one sig e52_53 extends Edge { } {
  ends = n52 + n53 }
one sig e52_54 extends Edge { } {
  ends = n52 + n54 }
one sig e52_60 extends Edge { } {
  ends = n52 + n60 }
one sig e53_55 extends Edge { } {
  ends = n53 + n55 }
one sig e53_61 extends Edge { } {
  ends = n53 + n61 }
one sig e54_55 extends Edge { } {
  ends = n54 + n55 }
one sig e54_62 extends Edge { } {
  ends = n54 + n62 }
one sig e55_63 extends Edge { } {
  ends = n55 + n63 }
one sig e56_57 extends Edge { } {
  ends = n56 + n57 }
one sig e56_58 extends Edge { } {
  ends = n56 + n58 }
one sig e56_60 extends Edge { } {
  ends = n56 + n60 }
one sig e57_59 extends Edge { } {
  ends = n57 + n59 }
one sig e57_61 extends Edge { } {
  ends = n57 + n61 }
one sig e58_59 extends Edge { } {
  ends = n58 + n59 }
one sig e58_62 extends Edge { } {
  ends = n58 + n62 }
one sig e59_63 extends Edge { } {
  ends = n59 + n63 }
one sig e60_61 extends Edge { } {
  ends = n60 + n61 }
one sig e60_62 extends Edge { } {
  ends = n60 + n62 }
one sig e61_63 extends Edge { } {
  ends = n61 + n63 }
one sig e62_63 extends Edge { } {
  ends = n62 + n63 }

one sig Q6 extends Graph { } {
  nodes = n0 + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10 + n11 + n12 + n13 + n14 + n15 + n16 + n17 + n18 + n19 + n20 + n21 + n22 + n23 + n24 + n25 + n26 + n27 + n28 + n29 + n30 + n31 + n32 + n33 + n34 + n35 + n36 + n37 + n38 + n39 + n40 + n41 + n42 + n43 + n44 + n45 + n46 + n47 + n48 + n49 + n50 + n51 + n52 + n53 + n54 + n55 + n56 + n57 + n58 + n59 + n60 + n61 + n62 + n63
  edges = e0_1 + e0_2 + e0_4 + e0_8 + e0_16 + e0_32 + e1_3 + e1_5 + e1_9 + e1_17 + e1_33 + e2_3 + e2_6 + e2_10 + e2_18 + e2_34 + e3_7 + e3_11 + e3_19 + e3_35 + e4_5 + e4_6 + e4_12 + e4_20 + e4_36 + e5_7 + e5_13 + e5_21 + e5_37 + e6_7 + e6_14 + e6_22 + e6_38 + e7_15 + e7_23 + e7_39 + e8_9 + e8_10 + e8_12 + e8_24 + e8_40 + e9_11 + e9_13 + e9_25 + e9_41 + e10_11 + e10_14 + e10_26 + e10_42 + e11_15 + e11_27 + e11_43 + e12_13 + e12_14 + e12_28 + e12_44 + e13_15 + e13_29 + e13_45 + e14_15 + e14_30 + e14_46 + e15_31 + e15_47 + e16_17 + e16_18 + e16_20 + e16_24 + e16_48 + e17_19 + e17_21 + e17_25 + e17_49 + e18_19 + e18_22 + e18_26 + e18_50 + e19_23 + e19_27 + e19_51 + e20_21 + e20_22 + e20_28 + e20_52 + e21_23 + e21_29 + e21_53 + e22_23 + e22_30 + e22_54 + e23_31 + e23_55 + e24_25 + e24_26 + e24_28 + e24_56 + e25_27 + e25_29 + e25_57 + e26_27 + e26_30 + e26_58 + e27_31 + e27_59 + e28_29 + e28_30 + e28_60 + e29_31 + e29_61 + e30_31 + e30_62 + e31_63 + e32_33 + e32_34 + e32_36 + e32_40 + e32_48 + e33_35 + e33_37 + e33_41 + e33_49 + e34_35 + e34_38 + e34_42 + e34_50 + e35_39 + e35_43 + e35_51 + e36_37 + e36_38 + e36_44 + e36_52 + e37_39 + e37_45 + e37_53 + e38_39 + e38_46 + e38_54 + e39_47 + e39_55 + e40_41 + e40_42 + e40_44 + e40_56 + e41_43 + e41_45 + e41_57 + e42_43 + e42_46 + e42_58 + e43_47 + e43_59 + e44_45 + e44_46 + e44_60 + e45_47 + e45_61 + e46_47 + e46_62 + e47_63 + e48_49 + e48_50 + e48_52 + e48_56 + e49_51 + e49_53 + e49_57 + e50_51 + e50_54 + e50_58 + e51_55 + e51_59 + e52_53 + e52_54 + e52_60 + e53_55 + e53_61 + e54_55 + e54_62 + e55_63 + e56_57 + e56_58 + e56_60 + e57_59 + e57_61 + e58_59 + e58_62 + e59_63 + e60_61 + e60_62 + e61_63 + e62_63
}
