sig Node  { }


sig Edge  {
  ends : set Node
} {
}

sig Graph  {
  nodes: set Node,
  edges: set Edge
} {
  edges.ends in nodes
}

//sig edge_set extends Graph { }
//{
// no nodes
//}





