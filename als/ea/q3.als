
open graph
open edgeAntipodal

pred antipodal[x : Graph]{
one (e0_1 + e6_7) & x.edges
one (e0_2 + e5_7) & x.edges
one (e0_4 + e3_7) & x.edges
one (e1_3 + e4_6) & x.edges
one (e1_5 + e2_6) & x.edges
one (e2_3 + e4_5) & x.edges
}

pred nonsimple[x: Graph]{
(e0_1 not in x.edges && e1_3 in x.edges && e2_3 not in x.edges && e0_2 in x.edges) || (e0_1 in x.edges && e1_3 not in x.edges && e2_3 in x.edges && e0_2 not in x.edges)||
(e4_5 not in x.edges && e5_7 in x.edges && e6_7 not in x.edges && e4_6 in x.edges) || (e4_5 in x.edges && e5_7 not in x.edges && e6_7 in x.edges && e4_6 not in x.edges)||
(e0_1 not in x.edges && e1_5 in x.edges && e4_5 not in x.edges && e0_4 in x.edges) || (e0_1 in x.edges && e1_5 not in x.edges && e4_5 in x.edges && e0_4 not in x.edges)||
(e2_3 not in x.edges && e3_7 in x.edges && e6_7 not in x.edges && e2_6 in x.edges) || (e2_3 in x.edges && e3_7 not in x.edges && e6_7 in x.edges && e2_6 not in x.edges)||
(e0_2 not in x.edges && e2_6 in x.edges && e4_6 not in x.edges && e0_4 in x.edges) || (e0_2 in x.edges && e2_6 not in x.edges && e4_6 in x.edges && e0_4 not in x.edges)||
(e1_3 not in x.edges && e3_7 in x.edges && e5_7 not in x.edges && e1_5 in x.edges) || (e1_3 in x.edges && e3_7 not in x.edges && e5_7 in x.edges && e1_5 not in x.edges)
}

pred antipodal_connected[x:Graph]{
connected[n0, n7, x] ||
connected[n1, n6, x] ||
connected[n2, n5, x] ||
connected[n3, n4, x]
}

one sig n0 extends Node { }
one sig n1 extends Node { }
one sig n2 extends Node { }
one sig n3 extends Node { }
one sig n4 extends Node { }
one sig n5 extends Node { }
one sig n6 extends Node { }
one sig n7 extends Node { }

one sig e0_1 extends Edge { } {
  src + dst = n0 + n1 }
one sig e1_0 extends Edge { } {
  src + dst = n0 + n1 }
one sig e0_2 extends Edge { } {
  src + dst = n0 + n2 }
one sig e2_0 extends Edge { } {
  src + dst = n0 + n2 }
one sig e0_4 extends Edge { } {
  src + dst = n0 + n4 }
one sig e4_0 extends Edge { } {
  src + dst = n0 + n4 }
one sig e1_3 extends Edge { } {
  src + dst = n1 + n3 }
one sig e3_1 extends Edge { } {
  src + dst = n1 + n3 }
one sig e1_5 extends Edge { } {
  src + dst = n1 + n5 }
one sig e5_1 extends Edge { } {
  src + dst = n1 + n5 }
one sig e2_3 extends Edge { } {
  src + dst = n2 + n3 }
one sig e3_2 extends Edge { } {
  src + dst = n2 + n3 }
one sig e2_6 extends Edge { } {
  src + dst = n2 + n6 }
one sig e6_2 extends Edge { } {
  src + dst = n2 + n6 }
one sig e3_7 extends Edge { } {
  src + dst = n3 + n7 }
one sig e7_3 extends Edge { } {
  src + dst = n3 + n7 }
one sig e4_5 extends Edge { } {
  src + dst = n4 + n5 }
one sig e5_4 extends Edge { } {
  src + dst = n4 + n5 }
one sig e4_6 extends Edge { } {
  src + dst = n4 + n6 }
one sig e6_4 extends Edge { } {
  src + dst = n4 + n6 }
one sig e5_7 extends Edge { } {
  src + dst = n5 + n7 }
one sig e7_5 extends Edge { } {
  src + dst = n5 + n7 }
one sig e6_7 extends Edge { } {
  src + dst = n6 + n7 }
one sig e7_6 extends Edge { } {
  src + dst = n6 + n7 }
one sig Q3 extends Graph { } {
  nodes = n0 + n1 + n2 + n3 + n4 + n5 + n6 + n7
  edges = e0_1 + e0_2 + e0_4 + e1_3 + e1_5 + e2_3 + e2_6 + e3_7 + e4_5 + e4_6 + e5_7 + e6_7 + e1_0 + e2_0 + e4_0 + e3_1 + e5_1 + e3_2 + e6_2 + e7_3 + e5_4 + e6_4 + e7_5 + e7_6
}
