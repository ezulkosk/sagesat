
open graph
open edgeAntipodal
pred antipodal[x : Graph]{
one (e0_1 + e62_63) & x.edges
one (e0_2 + e61_63) & x.edges
one (e0_4 + e59_63) & x.edges
one (e0_8 + e55_63) & x.edges
one (e0_16 + e47_63) & x.edges
one (e0_32 + e31_63) & x.edges
one (e1_3 + e60_62) & x.edges
one (e1_5 + e58_62) & x.edges
one (e1_9 + e54_62) & x.edges
one (e1_17 + e46_62) & x.edges
one (e1_33 + e30_62) & x.edges
one (e2_3 + e60_61) & x.edges
one (e2_6 + e57_61) & x.edges
one (e2_10 + e53_61) & x.edges
one (e2_18 + e45_61) & x.edges
one (e2_34 + e29_61) & x.edges
one (e3_7 + e56_60) & x.edges
one (e3_11 + e52_60) & x.edges
one (e3_19 + e44_60) & x.edges
one (e3_35 + e28_60) & x.edges
one (e4_5 + e58_59) & x.edges
one (e4_6 + e57_59) & x.edges
one (e4_12 + e51_59) & x.edges
one (e4_20 + e43_59) & x.edges
one (e4_36 + e27_59) & x.edges
one (e5_7 + e56_58) & x.edges
one (e5_13 + e50_58) & x.edges
one (e5_21 + e42_58) & x.edges
one (e5_37 + e26_58) & x.edges
one (e6_7 + e56_57) & x.edges
one (e6_14 + e49_57) & x.edges
one (e6_22 + e41_57) & x.edges
one (e6_38 + e25_57) & x.edges
one (e7_15 + e48_56) & x.edges
one (e7_23 + e40_56) & x.edges
one (e7_39 + e24_56) & x.edges
one (e8_9 + e54_55) & x.edges
one (e8_10 + e53_55) & x.edges
one (e8_12 + e51_55) & x.edges
one (e8_24 + e39_55) & x.edges
one (e8_40 + e23_55) & x.edges
one (e9_11 + e52_54) & x.edges
one (e9_13 + e50_54) & x.edges
one (e9_25 + e38_54) & x.edges
one (e9_41 + e22_54) & x.edges
one (e10_11 + e52_53) & x.edges
one (e10_14 + e49_53) & x.edges
one (e10_26 + e37_53) & x.edges
one (e10_42 + e21_53) & x.edges
one (e11_15 + e48_52) & x.edges
one (e11_27 + e36_52) & x.edges
one (e11_43 + e20_52) & x.edges
one (e12_13 + e50_51) & x.edges
one (e12_14 + e49_51) & x.edges
one (e12_28 + e35_51) & x.edges
one (e12_44 + e19_51) & x.edges
one (e13_15 + e48_50) & x.edges
one (e13_29 + e34_50) & x.edges
one (e13_45 + e18_50) & x.edges
one (e14_15 + e48_49) & x.edges
one (e14_30 + e33_49) & x.edges
one (e14_46 + e17_49) & x.edges
one (e15_31 + e32_48) & x.edges
one (e15_47 + e16_48) & x.edges
one (e16_17 + e46_47) & x.edges
one (e16_18 + e45_47) & x.edges
one (e16_20 + e43_47) & x.edges
one (e16_24 + e39_47) & x.edges
one (e17_19 + e44_46) & x.edges
one (e17_21 + e42_46) & x.edges
one (e17_25 + e38_46) & x.edges
one (e18_19 + e44_45) & x.edges
one (e18_22 + e41_45) & x.edges
one (e18_26 + e37_45) & x.edges
one (e19_23 + e40_44) & x.edges
one (e19_27 + e36_44) & x.edges
one (e20_21 + e42_43) & x.edges
one (e20_22 + e41_43) & x.edges
one (e20_28 + e35_43) & x.edges
one (e21_23 + e40_42) & x.edges
one (e21_29 + e34_42) & x.edges
one (e22_23 + e40_41) & x.edges
one (e22_30 + e33_41) & x.edges
one (e23_31 + e32_40) & x.edges
one (e24_25 + e38_39) & x.edges
one (e24_26 + e37_39) & x.edges
one (e24_28 + e35_39) & x.edges
one (e25_27 + e36_38) & x.edges
one (e25_29 + e34_38) & x.edges
one (e26_27 + e36_37) & x.edges
one (e26_30 + e33_37) & x.edges
one (e27_31 + e32_36) & x.edges
one (e28_29 + e34_35) & x.edges
one (e28_30 + e33_35) & x.edges
one (e29_31 + e32_34) & x.edges
one (e30_31 + e32_33) & x.edges
}

pred nonsimple[x: Graph]{
(e0_1 not in x.edges && e1_3 in x.edges && e2_3 not in x.edges && e0_2 in x.edges) || (e0_1 in x.edges && e1_3 not in x.edges && e2_3 in x.edges && e0_2 not in x.edges)||
(e4_5 not in x.edges && e5_7 in x.edges && e6_7 not in x.edges && e4_6 in x.edges) || (e4_5 in x.edges && e5_7 not in x.edges && e6_7 in x.edges && e4_6 not in x.edges)||
(e8_9 not in x.edges && e9_11 in x.edges && e10_11 not in x.edges && e8_10 in x.edges) || (e8_9 in x.edges && e9_11 not in x.edges && e10_11 in x.edges && e8_10 not in x.edges)||
(e12_13 not in x.edges && e13_15 in x.edges && e14_15 not in x.edges && e12_14 in x.edges) || (e12_13 in x.edges && e13_15 not in x.edges && e14_15 in x.edges && e12_14 not in x.edges)||
(e16_17 not in x.edges && e17_19 in x.edges && e18_19 not in x.edges && e16_18 in x.edges) || (e16_17 in x.edges && e17_19 not in x.edges && e18_19 in x.edges && e16_18 not in x.edges)||
(e20_21 not in x.edges && e21_23 in x.edges && e22_23 not in x.edges && e20_22 in x.edges) || (e20_21 in x.edges && e21_23 not in x.edges && e22_23 in x.edges && e20_22 not in x.edges)||
(e24_25 not in x.edges && e25_27 in x.edges && e26_27 not in x.edges && e24_26 in x.edges) || (e24_25 in x.edges && e25_27 not in x.edges && e26_27 in x.edges && e24_26 not in x.edges)||
(e28_29 not in x.edges && e29_31 in x.edges && e30_31 not in x.edges && e28_30 in x.edges) || (e28_29 in x.edges && e29_31 not in x.edges && e30_31 in x.edges && e28_30 not in x.edges)||
(e32_33 not in x.edges && e33_35 in x.edges && e34_35 not in x.edges && e32_34 in x.edges) || (e32_33 in x.edges && e33_35 not in x.edges && e34_35 in x.edges && e32_34 not in x.edges)||
(e36_37 not in x.edges && e37_39 in x.edges && e38_39 not in x.edges && e36_38 in x.edges) || (e36_37 in x.edges && e37_39 not in x.edges && e38_39 in x.edges && e36_38 not in x.edges)||
(e40_41 not in x.edges && e41_43 in x.edges && e42_43 not in x.edges && e40_42 in x.edges) || (e40_41 in x.edges && e41_43 not in x.edges && e42_43 in x.edges && e40_42 not in x.edges)||
(e44_45 not in x.edges && e45_47 in x.edges && e46_47 not in x.edges && e44_46 in x.edges) || (e44_45 in x.edges && e45_47 not in x.edges && e46_47 in x.edges && e44_46 not in x.edges)||
(e48_49 not in x.edges && e49_51 in x.edges && e50_51 not in x.edges && e48_50 in x.edges) || (e48_49 in x.edges && e49_51 not in x.edges && e50_51 in x.edges && e48_50 not in x.edges)||
(e52_53 not in x.edges && e53_55 in x.edges && e54_55 not in x.edges && e52_54 in x.edges) || (e52_53 in x.edges && e53_55 not in x.edges && e54_55 in x.edges && e52_54 not in x.edges)||
(e56_57 not in x.edges && e57_59 in x.edges && e58_59 not in x.edges && e56_58 in x.edges) || (e56_57 in x.edges && e57_59 not in x.edges && e58_59 in x.edges && e56_58 not in x.edges)||
(e60_61 not in x.edges && e61_63 in x.edges && e62_63 not in x.edges && e60_62 in x.edges) || (e60_61 in x.edges && e61_63 not in x.edges && e62_63 in x.edges && e60_62 not in x.edges)||
(e0_1 not in x.edges && e1_5 in x.edges && e4_5 not in x.edges && e0_4 in x.edges) || (e0_1 in x.edges && e1_5 not in x.edges && e4_5 in x.edges && e0_4 not in x.edges)||
(e2_3 not in x.edges && e3_7 in x.edges && e6_7 not in x.edges && e2_6 in x.edges) || (e2_3 in x.edges && e3_7 not in x.edges && e6_7 in x.edges && e2_6 not in x.edges)||
(e8_9 not in x.edges && e9_13 in x.edges && e12_13 not in x.edges && e8_12 in x.edges) || (e8_9 in x.edges && e9_13 not in x.edges && e12_13 in x.edges && e8_12 not in x.edges)||
(e10_11 not in x.edges && e11_15 in x.edges && e14_15 not in x.edges && e10_14 in x.edges) || (e10_11 in x.edges && e11_15 not in x.edges && e14_15 in x.edges && e10_14 not in x.edges)||
(e16_17 not in x.edges && e17_21 in x.edges && e20_21 not in x.edges && e16_20 in x.edges) || (e16_17 in x.edges && e17_21 not in x.edges && e20_21 in x.edges && e16_20 not in x.edges)||
(e18_19 not in x.edges && e19_23 in x.edges && e22_23 not in x.edges && e18_22 in x.edges) || (e18_19 in x.edges && e19_23 not in x.edges && e22_23 in x.edges && e18_22 not in x.edges)||
(e24_25 not in x.edges && e25_29 in x.edges && e28_29 not in x.edges && e24_28 in x.edges) || (e24_25 in x.edges && e25_29 not in x.edges && e28_29 in x.edges && e24_28 not in x.edges)||
(e26_27 not in x.edges && e27_31 in x.edges && e30_31 not in x.edges && e26_30 in x.edges) || (e26_27 in x.edges && e27_31 not in x.edges && e30_31 in x.edges && e26_30 not in x.edges)||
(e32_33 not in x.edges && e33_37 in x.edges && e36_37 not in x.edges && e32_36 in x.edges) || (e32_33 in x.edges && e33_37 not in x.edges && e36_37 in x.edges && e32_36 not in x.edges)||
(e34_35 not in x.edges && e35_39 in x.edges && e38_39 not in x.edges && e34_38 in x.edges) || (e34_35 in x.edges && e35_39 not in x.edges && e38_39 in x.edges && e34_38 not in x.edges)||
(e40_41 not in x.edges && e41_45 in x.edges && e44_45 not in x.edges && e40_44 in x.edges) || (e40_41 in x.edges && e41_45 not in x.edges && e44_45 in x.edges && e40_44 not in x.edges)||
(e42_43 not in x.edges && e43_47 in x.edges && e46_47 not in x.edges && e42_46 in x.edges) || (e42_43 in x.edges && e43_47 not in x.edges && e46_47 in x.edges && e42_46 not in x.edges)||
(e48_49 not in x.edges && e49_53 in x.edges && e52_53 not in x.edges && e48_52 in x.edges) || (e48_49 in x.edges && e49_53 not in x.edges && e52_53 in x.edges && e48_52 not in x.edges)||
(e50_51 not in x.edges && e51_55 in x.edges && e54_55 not in x.edges && e50_54 in x.edges) || (e50_51 in x.edges && e51_55 not in x.edges && e54_55 in x.edges && e50_54 not in x.edges)||
(e56_57 not in x.edges && e57_61 in x.edges && e60_61 not in x.edges && e56_60 in x.edges) || (e56_57 in x.edges && e57_61 not in x.edges && e60_61 in x.edges && e56_60 not in x.edges)||
(e58_59 not in x.edges && e59_63 in x.edges && e62_63 not in x.edges && e58_62 in x.edges) || (e58_59 in x.edges && e59_63 not in x.edges && e62_63 in x.edges && e58_62 not in x.edges)||
(e0_2 not in x.edges && e2_6 in x.edges && e4_6 not in x.edges && e0_4 in x.edges) || (e0_2 in x.edges && e2_6 not in x.edges && e4_6 in x.edges && e0_4 not in x.edges)||
(e1_3 not in x.edges && e3_7 in x.edges && e5_7 not in x.edges && e1_5 in x.edges) || (e1_3 in x.edges && e3_7 not in x.edges && e5_7 in x.edges && e1_5 not in x.edges)||
(e8_10 not in x.edges && e10_14 in x.edges && e12_14 not in x.edges && e8_12 in x.edges) || (e8_10 in x.edges && e10_14 not in x.edges && e12_14 in x.edges && e8_12 not in x.edges)||
(e9_11 not in x.edges && e11_15 in x.edges && e13_15 not in x.edges && e9_13 in x.edges) || (e9_11 in x.edges && e11_15 not in x.edges && e13_15 in x.edges && e9_13 not in x.edges)||
(e16_18 not in x.edges && e18_22 in x.edges && e20_22 not in x.edges && e16_20 in x.edges) || (e16_18 in x.edges && e18_22 not in x.edges && e20_22 in x.edges && e16_20 not in x.edges)||
(e17_19 not in x.edges && e19_23 in x.edges && e21_23 not in x.edges && e17_21 in x.edges) || (e17_19 in x.edges && e19_23 not in x.edges && e21_23 in x.edges && e17_21 not in x.edges)||
(e24_26 not in x.edges && e26_30 in x.edges && e28_30 not in x.edges && e24_28 in x.edges) || (e24_26 in x.edges && e26_30 not in x.edges && e28_30 in x.edges && e24_28 not in x.edges)||
(e25_27 not in x.edges && e27_31 in x.edges && e29_31 not in x.edges && e25_29 in x.edges) || (e25_27 in x.edges && e27_31 not in x.edges && e29_31 in x.edges && e25_29 not in x.edges)||
(e32_34 not in x.edges && e34_38 in x.edges && e36_38 not in x.edges && e32_36 in x.edges) || (e32_34 in x.edges && e34_38 not in x.edges && e36_38 in x.edges && e32_36 not in x.edges)||
(e33_35 not in x.edges && e35_39 in x.edges && e37_39 not in x.edges && e33_37 in x.edges) || (e33_35 in x.edges && e35_39 not in x.edges && e37_39 in x.edges && e33_37 not in x.edges)||
(e40_42 not in x.edges && e42_46 in x.edges && e44_46 not in x.edges && e40_44 in x.edges) || (e40_42 in x.edges && e42_46 not in x.edges && e44_46 in x.edges && e40_44 not in x.edges)||
(e41_43 not in x.edges && e43_47 in x.edges && e45_47 not in x.edges && e41_45 in x.edges) || (e41_43 in x.edges && e43_47 not in x.edges && e45_47 in x.edges && e41_45 not in x.edges)||
(e48_50 not in x.edges && e50_54 in x.edges && e52_54 not in x.edges && e48_52 in x.edges) || (e48_50 in x.edges && e50_54 not in x.edges && e52_54 in x.edges && e48_52 not in x.edges)||
(e49_51 not in x.edges && e51_55 in x.edges && e53_55 not in x.edges && e49_53 in x.edges) || (e49_51 in x.edges && e51_55 not in x.edges && e53_55 in x.edges && e49_53 not in x.edges)||
(e56_58 not in x.edges && e58_62 in x.edges && e60_62 not in x.edges && e56_60 in x.edges) || (e56_58 in x.edges && e58_62 not in x.edges && e60_62 in x.edges && e56_60 not in x.edges)||
(e57_59 not in x.edges && e59_63 in x.edges && e61_63 not in x.edges && e57_61 in x.edges) || (e57_59 in x.edges && e59_63 not in x.edges && e61_63 in x.edges && e57_61 not in x.edges)||
(e0_1 not in x.edges && e1_9 in x.edges && e8_9 not in x.edges && e0_8 in x.edges) || (e0_1 in x.edges && e1_9 not in x.edges && e8_9 in x.edges && e0_8 not in x.edges)||
(e2_3 not in x.edges && e3_11 in x.edges && e10_11 not in x.edges && e2_10 in x.edges) || (e2_3 in x.edges && e3_11 not in x.edges && e10_11 in x.edges && e2_10 not in x.edges)||
(e4_5 not in x.edges && e5_13 in x.edges && e12_13 not in x.edges && e4_12 in x.edges) || (e4_5 in x.edges && e5_13 not in x.edges && e12_13 in x.edges && e4_12 not in x.edges)||
(e6_7 not in x.edges && e7_15 in x.edges && e14_15 not in x.edges && e6_14 in x.edges) || (e6_7 in x.edges && e7_15 not in x.edges && e14_15 in x.edges && e6_14 not in x.edges)||
(e16_17 not in x.edges && e17_25 in x.edges && e24_25 not in x.edges && e16_24 in x.edges) || (e16_17 in x.edges && e17_25 not in x.edges && e24_25 in x.edges && e16_24 not in x.edges)||
(e18_19 not in x.edges && e19_27 in x.edges && e26_27 not in x.edges && e18_26 in x.edges) || (e18_19 in x.edges && e19_27 not in x.edges && e26_27 in x.edges && e18_26 not in x.edges)||
(e20_21 not in x.edges && e21_29 in x.edges && e28_29 not in x.edges && e20_28 in x.edges) || (e20_21 in x.edges && e21_29 not in x.edges && e28_29 in x.edges && e20_28 not in x.edges)||
(e22_23 not in x.edges && e23_31 in x.edges && e30_31 not in x.edges && e22_30 in x.edges) || (e22_23 in x.edges && e23_31 not in x.edges && e30_31 in x.edges && e22_30 not in x.edges)||
(e32_33 not in x.edges && e33_41 in x.edges && e40_41 not in x.edges && e32_40 in x.edges) || (e32_33 in x.edges && e33_41 not in x.edges && e40_41 in x.edges && e32_40 not in x.edges)||
(e34_35 not in x.edges && e35_43 in x.edges && e42_43 not in x.edges && e34_42 in x.edges) || (e34_35 in x.edges && e35_43 not in x.edges && e42_43 in x.edges && e34_42 not in x.edges)||
(e36_37 not in x.edges && e37_45 in x.edges && e44_45 not in x.edges && e36_44 in x.edges) || (e36_37 in x.edges && e37_45 not in x.edges && e44_45 in x.edges && e36_44 not in x.edges)||
(e38_39 not in x.edges && e39_47 in x.edges && e46_47 not in x.edges && e38_46 in x.edges) || (e38_39 in x.edges && e39_47 not in x.edges && e46_47 in x.edges && e38_46 not in x.edges)||
(e48_49 not in x.edges && e49_57 in x.edges && e56_57 not in x.edges && e48_56 in x.edges) || (e48_49 in x.edges && e49_57 not in x.edges && e56_57 in x.edges && e48_56 not in x.edges)||
(e50_51 not in x.edges && e51_59 in x.edges && e58_59 not in x.edges && e50_58 in x.edges) || (e50_51 in x.edges && e51_59 not in x.edges && e58_59 in x.edges && e50_58 not in x.edges)||
(e52_53 not in x.edges && e53_61 in x.edges && e60_61 not in x.edges && e52_60 in x.edges) || (e52_53 in x.edges && e53_61 not in x.edges && e60_61 in x.edges && e52_60 not in x.edges)||
(e54_55 not in x.edges && e55_63 in x.edges && e62_63 not in x.edges && e54_62 in x.edges) || (e54_55 in x.edges && e55_63 not in x.edges && e62_63 in x.edges && e54_62 not in x.edges)||
(e0_2 not in x.edges && e2_10 in x.edges && e8_10 not in x.edges && e0_8 in x.edges) || (e0_2 in x.edges && e2_10 not in x.edges && e8_10 in x.edges && e0_8 not in x.edges)||
(e1_3 not in x.edges && e3_11 in x.edges && e9_11 not in x.edges && e1_9 in x.edges) || (e1_3 in x.edges && e3_11 not in x.edges && e9_11 in x.edges && e1_9 not in x.edges)||
(e4_6 not in x.edges && e6_14 in x.edges && e12_14 not in x.edges && e4_12 in x.edges) || (e4_6 in x.edges && e6_14 not in x.edges && e12_14 in x.edges && e4_12 not in x.edges)||
(e5_7 not in x.edges && e7_15 in x.edges && e13_15 not in x.edges && e5_13 in x.edges) || (e5_7 in x.edges && e7_15 not in x.edges && e13_15 in x.edges && e5_13 not in x.edges)||
(e16_18 not in x.edges && e18_26 in x.edges && e24_26 not in x.edges && e16_24 in x.edges) || (e16_18 in x.edges && e18_26 not in x.edges && e24_26 in x.edges && e16_24 not in x.edges)||
(e17_19 not in x.edges && e19_27 in x.edges && e25_27 not in x.edges && e17_25 in x.edges) || (e17_19 in x.edges && e19_27 not in x.edges && e25_27 in x.edges && e17_25 not in x.edges)||
(e20_22 not in x.edges && e22_30 in x.edges && e28_30 not in x.edges && e20_28 in x.edges) || (e20_22 in x.edges && e22_30 not in x.edges && e28_30 in x.edges && e20_28 not in x.edges)||
(e21_23 not in x.edges && e23_31 in x.edges && e29_31 not in x.edges && e21_29 in x.edges) || (e21_23 in x.edges && e23_31 not in x.edges && e29_31 in x.edges && e21_29 not in x.edges)||
(e32_34 not in x.edges && e34_42 in x.edges && e40_42 not in x.edges && e32_40 in x.edges) || (e32_34 in x.edges && e34_42 not in x.edges && e40_42 in x.edges && e32_40 not in x.edges)||
(e33_35 not in x.edges && e35_43 in x.edges && e41_43 not in x.edges && e33_41 in x.edges) || (e33_35 in x.edges && e35_43 not in x.edges && e41_43 in x.edges && e33_41 not in x.edges)||
(e36_38 not in x.edges && e38_46 in x.edges && e44_46 not in x.edges && e36_44 in x.edges) || (e36_38 in x.edges && e38_46 not in x.edges && e44_46 in x.edges && e36_44 not in x.edges)||
(e37_39 not in x.edges && e39_47 in x.edges && e45_47 not in x.edges && e37_45 in x.edges) || (e37_39 in x.edges && e39_47 not in x.edges && e45_47 in x.edges && e37_45 not in x.edges)||
(e48_50 not in x.edges && e50_58 in x.edges && e56_58 not in x.edges && e48_56 in x.edges) || (e48_50 in x.edges && e50_58 not in x.edges && e56_58 in x.edges && e48_56 not in x.edges)||
(e49_51 not in x.edges && e51_59 in x.edges && e57_59 not in x.edges && e49_57 in x.edges) || (e49_51 in x.edges && e51_59 not in x.edges && e57_59 in x.edges && e49_57 not in x.edges)||
(e52_54 not in x.edges && e54_62 in x.edges && e60_62 not in x.edges && e52_60 in x.edges) || (e52_54 in x.edges && e54_62 not in x.edges && e60_62 in x.edges && e52_60 not in x.edges)||
(e53_55 not in x.edges && e55_63 in x.edges && e61_63 not in x.edges && e53_61 in x.edges) || (e53_55 in x.edges && e55_63 not in x.edges && e61_63 in x.edges && e53_61 not in x.edges)||
(e0_4 not in x.edges && e4_12 in x.edges && e8_12 not in x.edges && e0_8 in x.edges) || (e0_4 in x.edges && e4_12 not in x.edges && e8_12 in x.edges && e0_8 not in x.edges)||
(e1_5 not in x.edges && e5_13 in x.edges && e9_13 not in x.edges && e1_9 in x.edges) || (e1_5 in x.edges && e5_13 not in x.edges && e9_13 in x.edges && e1_9 not in x.edges)||
(e2_6 not in x.edges && e6_14 in x.edges && e10_14 not in x.edges && e2_10 in x.edges) || (e2_6 in x.edges && e6_14 not in x.edges && e10_14 in x.edges && e2_10 not in x.edges)||
(e3_7 not in x.edges && e7_15 in x.edges && e11_15 not in x.edges && e3_11 in x.edges) || (e3_7 in x.edges && e7_15 not in x.edges && e11_15 in x.edges && e3_11 not in x.edges)||
(e16_20 not in x.edges && e20_28 in x.edges && e24_28 not in x.edges && e16_24 in x.edges) || (e16_20 in x.edges && e20_28 not in x.edges && e24_28 in x.edges && e16_24 not in x.edges)||
(e17_21 not in x.edges && e21_29 in x.edges && e25_29 not in x.edges && e17_25 in x.edges) || (e17_21 in x.edges && e21_29 not in x.edges && e25_29 in x.edges && e17_25 not in x.edges)||
(e18_22 not in x.edges && e22_30 in x.edges && e26_30 not in x.edges && e18_26 in x.edges) || (e18_22 in x.edges && e22_30 not in x.edges && e26_30 in x.edges && e18_26 not in x.edges)||
(e19_23 not in x.edges && e23_31 in x.edges && e27_31 not in x.edges && e19_27 in x.edges) || (e19_23 in x.edges && e23_31 not in x.edges && e27_31 in x.edges && e19_27 not in x.edges)||
(e32_36 not in x.edges && e36_44 in x.edges && e40_44 not in x.edges && e32_40 in x.edges) || (e32_36 in x.edges && e36_44 not in x.edges && e40_44 in x.edges && e32_40 not in x.edges)||
(e33_37 not in x.edges && e37_45 in x.edges && e41_45 not in x.edges && e33_41 in x.edges) || (e33_37 in x.edges && e37_45 not in x.edges && e41_45 in x.edges && e33_41 not in x.edges)||
(e34_38 not in x.edges && e38_46 in x.edges && e42_46 not in x.edges && e34_42 in x.edges) || (e34_38 in x.edges && e38_46 not in x.edges && e42_46 in x.edges && e34_42 not in x.edges)||
(e35_39 not in x.edges && e39_47 in x.edges && e43_47 not in x.edges && e35_43 in x.edges) || (e35_39 in x.edges && e39_47 not in x.edges && e43_47 in x.edges && e35_43 not in x.edges)||
(e48_52 not in x.edges && e52_60 in x.edges && e56_60 not in x.edges && e48_56 in x.edges) || (e48_52 in x.edges && e52_60 not in x.edges && e56_60 in x.edges && e48_56 not in x.edges)||
(e49_53 not in x.edges && e53_61 in x.edges && e57_61 not in x.edges && e49_57 in x.edges) || (e49_53 in x.edges && e53_61 not in x.edges && e57_61 in x.edges && e49_57 not in x.edges)||
(e50_54 not in x.edges && e54_62 in x.edges && e58_62 not in x.edges && e50_58 in x.edges) || (e50_54 in x.edges && e54_62 not in x.edges && e58_62 in x.edges && e50_58 not in x.edges)||
(e51_55 not in x.edges && e55_63 in x.edges && e59_63 not in x.edges && e51_59 in x.edges) || (e51_55 in x.edges && e55_63 not in x.edges && e59_63 in x.edges && e51_59 not in x.edges)||
(e0_1 not in x.edges && e1_17 in x.edges && e16_17 not in x.edges && e0_16 in x.edges) || (e0_1 in x.edges && e1_17 not in x.edges && e16_17 in x.edges && e0_16 not in x.edges)||
(e2_3 not in x.edges && e3_19 in x.edges && e18_19 not in x.edges && e2_18 in x.edges) || (e2_3 in x.edges && e3_19 not in x.edges && e18_19 in x.edges && e2_18 not in x.edges)||
(e4_5 not in x.edges && e5_21 in x.edges && e20_21 not in x.edges && e4_20 in x.edges) || (e4_5 in x.edges && e5_21 not in x.edges && e20_21 in x.edges && e4_20 not in x.edges)||
(e6_7 not in x.edges && e7_23 in x.edges && e22_23 not in x.edges && e6_22 in x.edges) || (e6_7 in x.edges && e7_23 not in x.edges && e22_23 in x.edges && e6_22 not in x.edges)||
(e8_9 not in x.edges && e9_25 in x.edges && e24_25 not in x.edges && e8_24 in x.edges) || (e8_9 in x.edges && e9_25 not in x.edges && e24_25 in x.edges && e8_24 not in x.edges)||
(e10_11 not in x.edges && e11_27 in x.edges && e26_27 not in x.edges && e10_26 in x.edges) || (e10_11 in x.edges && e11_27 not in x.edges && e26_27 in x.edges && e10_26 not in x.edges)||
(e12_13 not in x.edges && e13_29 in x.edges && e28_29 not in x.edges && e12_28 in x.edges) || (e12_13 in x.edges && e13_29 not in x.edges && e28_29 in x.edges && e12_28 not in x.edges)||
(e14_15 not in x.edges && e15_31 in x.edges && e30_31 not in x.edges && e14_30 in x.edges) || (e14_15 in x.edges && e15_31 not in x.edges && e30_31 in x.edges && e14_30 not in x.edges)||
(e32_33 not in x.edges && e33_49 in x.edges && e48_49 not in x.edges && e32_48 in x.edges) || (e32_33 in x.edges && e33_49 not in x.edges && e48_49 in x.edges && e32_48 not in x.edges)||
(e34_35 not in x.edges && e35_51 in x.edges && e50_51 not in x.edges && e34_50 in x.edges) || (e34_35 in x.edges && e35_51 not in x.edges && e50_51 in x.edges && e34_50 not in x.edges)||
(e36_37 not in x.edges && e37_53 in x.edges && e52_53 not in x.edges && e36_52 in x.edges) || (e36_37 in x.edges && e37_53 not in x.edges && e52_53 in x.edges && e36_52 not in x.edges)||
(e38_39 not in x.edges && e39_55 in x.edges && e54_55 not in x.edges && e38_54 in x.edges) || (e38_39 in x.edges && e39_55 not in x.edges && e54_55 in x.edges && e38_54 not in x.edges)||
(e40_41 not in x.edges && e41_57 in x.edges && e56_57 not in x.edges && e40_56 in x.edges) || (e40_41 in x.edges && e41_57 not in x.edges && e56_57 in x.edges && e40_56 not in x.edges)||
(e42_43 not in x.edges && e43_59 in x.edges && e58_59 not in x.edges && e42_58 in x.edges) || (e42_43 in x.edges && e43_59 not in x.edges && e58_59 in x.edges && e42_58 not in x.edges)||
(e44_45 not in x.edges && e45_61 in x.edges && e60_61 not in x.edges && e44_60 in x.edges) || (e44_45 in x.edges && e45_61 not in x.edges && e60_61 in x.edges && e44_60 not in x.edges)||
(e46_47 not in x.edges && e47_63 in x.edges && e62_63 not in x.edges && e46_62 in x.edges) || (e46_47 in x.edges && e47_63 not in x.edges && e62_63 in x.edges && e46_62 not in x.edges)||
(e0_2 not in x.edges && e2_18 in x.edges && e16_18 not in x.edges && e0_16 in x.edges) || (e0_2 in x.edges && e2_18 not in x.edges && e16_18 in x.edges && e0_16 not in x.edges)||
(e1_3 not in x.edges && e3_19 in x.edges && e17_19 not in x.edges && e1_17 in x.edges) || (e1_3 in x.edges && e3_19 not in x.edges && e17_19 in x.edges && e1_17 not in x.edges)||
(e4_6 not in x.edges && e6_22 in x.edges && e20_22 not in x.edges && e4_20 in x.edges) || (e4_6 in x.edges && e6_22 not in x.edges && e20_22 in x.edges && e4_20 not in x.edges)||
(e5_7 not in x.edges && e7_23 in x.edges && e21_23 not in x.edges && e5_21 in x.edges) || (e5_7 in x.edges && e7_23 not in x.edges && e21_23 in x.edges && e5_21 not in x.edges)||
(e8_10 not in x.edges && e10_26 in x.edges && e24_26 not in x.edges && e8_24 in x.edges) || (e8_10 in x.edges && e10_26 not in x.edges && e24_26 in x.edges && e8_24 not in x.edges)||
(e9_11 not in x.edges && e11_27 in x.edges && e25_27 not in x.edges && e9_25 in x.edges) || (e9_11 in x.edges && e11_27 not in x.edges && e25_27 in x.edges && e9_25 not in x.edges)||
(e12_14 not in x.edges && e14_30 in x.edges && e28_30 not in x.edges && e12_28 in x.edges) || (e12_14 in x.edges && e14_30 not in x.edges && e28_30 in x.edges && e12_28 not in x.edges)||
(e13_15 not in x.edges && e15_31 in x.edges && e29_31 not in x.edges && e13_29 in x.edges) || (e13_15 in x.edges && e15_31 not in x.edges && e29_31 in x.edges && e13_29 not in x.edges)||
(e32_34 not in x.edges && e34_50 in x.edges && e48_50 not in x.edges && e32_48 in x.edges) || (e32_34 in x.edges && e34_50 not in x.edges && e48_50 in x.edges && e32_48 not in x.edges)||
(e33_35 not in x.edges && e35_51 in x.edges && e49_51 not in x.edges && e33_49 in x.edges) || (e33_35 in x.edges && e35_51 not in x.edges && e49_51 in x.edges && e33_49 not in x.edges)||
(e36_38 not in x.edges && e38_54 in x.edges && e52_54 not in x.edges && e36_52 in x.edges) || (e36_38 in x.edges && e38_54 not in x.edges && e52_54 in x.edges && e36_52 not in x.edges)||
(e37_39 not in x.edges && e39_55 in x.edges && e53_55 not in x.edges && e37_53 in x.edges) || (e37_39 in x.edges && e39_55 not in x.edges && e53_55 in x.edges && e37_53 not in x.edges)||
(e40_42 not in x.edges && e42_58 in x.edges && e56_58 not in x.edges && e40_56 in x.edges) || (e40_42 in x.edges && e42_58 not in x.edges && e56_58 in x.edges && e40_56 not in x.edges)||
(e41_43 not in x.edges && e43_59 in x.edges && e57_59 not in x.edges && e41_57 in x.edges) || (e41_43 in x.edges && e43_59 not in x.edges && e57_59 in x.edges && e41_57 not in x.edges)||
(e44_46 not in x.edges && e46_62 in x.edges && e60_62 not in x.edges && e44_60 in x.edges) || (e44_46 in x.edges && e46_62 not in x.edges && e60_62 in x.edges && e44_60 not in x.edges)||
(e45_47 not in x.edges && e47_63 in x.edges && e61_63 not in x.edges && e45_61 in x.edges) || (e45_47 in x.edges && e47_63 not in x.edges && e61_63 in x.edges && e45_61 not in x.edges)||
(e0_4 not in x.edges && e4_20 in x.edges && e16_20 not in x.edges && e0_16 in x.edges) || (e0_4 in x.edges && e4_20 not in x.edges && e16_20 in x.edges && e0_16 not in x.edges)||
(e1_5 not in x.edges && e5_21 in x.edges && e17_21 not in x.edges && e1_17 in x.edges) || (e1_5 in x.edges && e5_21 not in x.edges && e17_21 in x.edges && e1_17 not in x.edges)||
(e2_6 not in x.edges && e6_22 in x.edges && e18_22 not in x.edges && e2_18 in x.edges) || (e2_6 in x.edges && e6_22 not in x.edges && e18_22 in x.edges && e2_18 not in x.edges)||
(e3_7 not in x.edges && e7_23 in x.edges && e19_23 not in x.edges && e3_19 in x.edges) || (e3_7 in x.edges && e7_23 not in x.edges && e19_23 in x.edges && e3_19 not in x.edges)||
(e8_12 not in x.edges && e12_28 in x.edges && e24_28 not in x.edges && e8_24 in x.edges) || (e8_12 in x.edges && e12_28 not in x.edges && e24_28 in x.edges && e8_24 not in x.edges)||
(e9_13 not in x.edges && e13_29 in x.edges && e25_29 not in x.edges && e9_25 in x.edges) || (e9_13 in x.edges && e13_29 not in x.edges && e25_29 in x.edges && e9_25 not in x.edges)||
(e10_14 not in x.edges && e14_30 in x.edges && e26_30 not in x.edges && e10_26 in x.edges) || (e10_14 in x.edges && e14_30 not in x.edges && e26_30 in x.edges && e10_26 not in x.edges)||
(e11_15 not in x.edges && e15_31 in x.edges && e27_31 not in x.edges && e11_27 in x.edges) || (e11_15 in x.edges && e15_31 not in x.edges && e27_31 in x.edges && e11_27 not in x.edges)||
(e32_36 not in x.edges && e36_52 in x.edges && e48_52 not in x.edges && e32_48 in x.edges) || (e32_36 in x.edges && e36_52 not in x.edges && e48_52 in x.edges && e32_48 not in x.edges)||
(e33_37 not in x.edges && e37_53 in x.edges && e49_53 not in x.edges && e33_49 in x.edges) || (e33_37 in x.edges && e37_53 not in x.edges && e49_53 in x.edges && e33_49 not in x.edges)||
(e34_38 not in x.edges && e38_54 in x.edges && e50_54 not in x.edges && e34_50 in x.edges) || (e34_38 in x.edges && e38_54 not in x.edges && e50_54 in x.edges && e34_50 not in x.edges)||
(e35_39 not in x.edges && e39_55 in x.edges && e51_55 not in x.edges && e35_51 in x.edges) || (e35_39 in x.edges && e39_55 not in x.edges && e51_55 in x.edges && e35_51 not in x.edges)||
(e40_44 not in x.edges && e44_60 in x.edges && e56_60 not in x.edges && e40_56 in x.edges) || (e40_44 in x.edges && e44_60 not in x.edges && e56_60 in x.edges && e40_56 not in x.edges)||
(e41_45 not in x.edges && e45_61 in x.edges && e57_61 not in x.edges && e41_57 in x.edges) || (e41_45 in x.edges && e45_61 not in x.edges && e57_61 in x.edges && e41_57 not in x.edges)||
(e42_46 not in x.edges && e46_62 in x.edges && e58_62 not in x.edges && e42_58 in x.edges) || (e42_46 in x.edges && e46_62 not in x.edges && e58_62 in x.edges && e42_58 not in x.edges)||
(e43_47 not in x.edges && e47_63 in x.edges && e59_63 not in x.edges && e43_59 in x.edges) || (e43_47 in x.edges && e47_63 not in x.edges && e59_63 in x.edges && e43_59 not in x.edges)||
(e0_8 not in x.edges && e8_24 in x.edges && e16_24 not in x.edges && e0_16 in x.edges) || (e0_8 in x.edges && e8_24 not in x.edges && e16_24 in x.edges && e0_16 not in x.edges)||
(e1_9 not in x.edges && e9_25 in x.edges && e17_25 not in x.edges && e1_17 in x.edges) || (e1_9 in x.edges && e9_25 not in x.edges && e17_25 in x.edges && e1_17 not in x.edges)||
(e2_10 not in x.edges && e10_26 in x.edges && e18_26 not in x.edges && e2_18 in x.edges) || (e2_10 in x.edges && e10_26 not in x.edges && e18_26 in x.edges && e2_18 not in x.edges)||
(e3_11 not in x.edges && e11_27 in x.edges && e19_27 not in x.edges && e3_19 in x.edges) || (e3_11 in x.edges && e11_27 not in x.edges && e19_27 in x.edges && e3_19 not in x.edges)||
(e4_12 not in x.edges && e12_28 in x.edges && e20_28 not in x.edges && e4_20 in x.edges) || (e4_12 in x.edges && e12_28 not in x.edges && e20_28 in x.edges && e4_20 not in x.edges)||
(e5_13 not in x.edges && e13_29 in x.edges && e21_29 not in x.edges && e5_21 in x.edges) || (e5_13 in x.edges && e13_29 not in x.edges && e21_29 in x.edges && e5_21 not in x.edges)||
(e6_14 not in x.edges && e14_30 in x.edges && e22_30 not in x.edges && e6_22 in x.edges) || (e6_14 in x.edges && e14_30 not in x.edges && e22_30 in x.edges && e6_22 not in x.edges)||
(e7_15 not in x.edges && e15_31 in x.edges && e23_31 not in x.edges && e7_23 in x.edges) || (e7_15 in x.edges && e15_31 not in x.edges && e23_31 in x.edges && e7_23 not in x.edges)||
(e32_40 not in x.edges && e40_56 in x.edges && e48_56 not in x.edges && e32_48 in x.edges) || (e32_40 in x.edges && e40_56 not in x.edges && e48_56 in x.edges && e32_48 not in x.edges)||
(e33_41 not in x.edges && e41_57 in x.edges && e49_57 not in x.edges && e33_49 in x.edges) || (e33_41 in x.edges && e41_57 not in x.edges && e49_57 in x.edges && e33_49 not in x.edges)||
(e34_42 not in x.edges && e42_58 in x.edges && e50_58 not in x.edges && e34_50 in x.edges) || (e34_42 in x.edges && e42_58 not in x.edges && e50_58 in x.edges && e34_50 not in x.edges)||
(e35_43 not in x.edges && e43_59 in x.edges && e51_59 not in x.edges && e35_51 in x.edges) || (e35_43 in x.edges && e43_59 not in x.edges && e51_59 in x.edges && e35_51 not in x.edges)||
(e36_44 not in x.edges && e44_60 in x.edges && e52_60 not in x.edges && e36_52 in x.edges) || (e36_44 in x.edges && e44_60 not in x.edges && e52_60 in x.edges && e36_52 not in x.edges)||
(e37_45 not in x.edges && e45_61 in x.edges && e53_61 not in x.edges && e37_53 in x.edges) || (e37_45 in x.edges && e45_61 not in x.edges && e53_61 in x.edges && e37_53 not in x.edges)||
(e38_46 not in x.edges && e46_62 in x.edges && e54_62 not in x.edges && e38_54 in x.edges) || (e38_46 in x.edges && e46_62 not in x.edges && e54_62 in x.edges && e38_54 not in x.edges)||
(e39_47 not in x.edges && e47_63 in x.edges && e55_63 not in x.edges && e39_55 in x.edges) || (e39_47 in x.edges && e47_63 not in x.edges && e55_63 in x.edges && e39_55 not in x.edges)||
(e0_1 not in x.edges && e1_33 in x.edges && e32_33 not in x.edges && e0_32 in x.edges) || (e0_1 in x.edges && e1_33 not in x.edges && e32_33 in x.edges && e0_32 not in x.edges)||
(e2_3 not in x.edges && e3_35 in x.edges && e34_35 not in x.edges && e2_34 in x.edges) || (e2_3 in x.edges && e3_35 not in x.edges && e34_35 in x.edges && e2_34 not in x.edges)||
(e4_5 not in x.edges && e5_37 in x.edges && e36_37 not in x.edges && e4_36 in x.edges) || (e4_5 in x.edges && e5_37 not in x.edges && e36_37 in x.edges && e4_36 not in x.edges)||
(e6_7 not in x.edges && e7_39 in x.edges && e38_39 not in x.edges && e6_38 in x.edges) || (e6_7 in x.edges && e7_39 not in x.edges && e38_39 in x.edges && e6_38 not in x.edges)||
(e8_9 not in x.edges && e9_41 in x.edges && e40_41 not in x.edges && e8_40 in x.edges) || (e8_9 in x.edges && e9_41 not in x.edges && e40_41 in x.edges && e8_40 not in x.edges)||
(e10_11 not in x.edges && e11_43 in x.edges && e42_43 not in x.edges && e10_42 in x.edges) || (e10_11 in x.edges && e11_43 not in x.edges && e42_43 in x.edges && e10_42 not in x.edges)||
(e12_13 not in x.edges && e13_45 in x.edges && e44_45 not in x.edges && e12_44 in x.edges) || (e12_13 in x.edges && e13_45 not in x.edges && e44_45 in x.edges && e12_44 not in x.edges)||
(e14_15 not in x.edges && e15_47 in x.edges && e46_47 not in x.edges && e14_46 in x.edges) || (e14_15 in x.edges && e15_47 not in x.edges && e46_47 in x.edges && e14_46 not in x.edges)||
(e16_17 not in x.edges && e17_49 in x.edges && e48_49 not in x.edges && e16_48 in x.edges) || (e16_17 in x.edges && e17_49 not in x.edges && e48_49 in x.edges && e16_48 not in x.edges)||
(e18_19 not in x.edges && e19_51 in x.edges && e50_51 not in x.edges && e18_50 in x.edges) || (e18_19 in x.edges && e19_51 not in x.edges && e50_51 in x.edges && e18_50 not in x.edges)||
(e20_21 not in x.edges && e21_53 in x.edges && e52_53 not in x.edges && e20_52 in x.edges) || (e20_21 in x.edges && e21_53 not in x.edges && e52_53 in x.edges && e20_52 not in x.edges)||
(e22_23 not in x.edges && e23_55 in x.edges && e54_55 not in x.edges && e22_54 in x.edges) || (e22_23 in x.edges && e23_55 not in x.edges && e54_55 in x.edges && e22_54 not in x.edges)||
(e24_25 not in x.edges && e25_57 in x.edges && e56_57 not in x.edges && e24_56 in x.edges) || (e24_25 in x.edges && e25_57 not in x.edges && e56_57 in x.edges && e24_56 not in x.edges)||
(e26_27 not in x.edges && e27_59 in x.edges && e58_59 not in x.edges && e26_58 in x.edges) || (e26_27 in x.edges && e27_59 not in x.edges && e58_59 in x.edges && e26_58 not in x.edges)||
(e28_29 not in x.edges && e29_61 in x.edges && e60_61 not in x.edges && e28_60 in x.edges) || (e28_29 in x.edges && e29_61 not in x.edges && e60_61 in x.edges && e28_60 not in x.edges)||
(e30_31 not in x.edges && e31_63 in x.edges && e62_63 not in x.edges && e30_62 in x.edges) || (e30_31 in x.edges && e31_63 not in x.edges && e62_63 in x.edges && e30_62 not in x.edges)||
(e0_2 not in x.edges && e2_34 in x.edges && e32_34 not in x.edges && e0_32 in x.edges) || (e0_2 in x.edges && e2_34 not in x.edges && e32_34 in x.edges && e0_32 not in x.edges)||
(e1_3 not in x.edges && e3_35 in x.edges && e33_35 not in x.edges && e1_33 in x.edges) || (e1_3 in x.edges && e3_35 not in x.edges && e33_35 in x.edges && e1_33 not in x.edges)||
(e4_6 not in x.edges && e6_38 in x.edges && e36_38 not in x.edges && e4_36 in x.edges) || (e4_6 in x.edges && e6_38 not in x.edges && e36_38 in x.edges && e4_36 not in x.edges)||
(e5_7 not in x.edges && e7_39 in x.edges && e37_39 not in x.edges && e5_37 in x.edges) || (e5_7 in x.edges && e7_39 not in x.edges && e37_39 in x.edges && e5_37 not in x.edges)||
(e8_10 not in x.edges && e10_42 in x.edges && e40_42 not in x.edges && e8_40 in x.edges) || (e8_10 in x.edges && e10_42 not in x.edges && e40_42 in x.edges && e8_40 not in x.edges)||
(e9_11 not in x.edges && e11_43 in x.edges && e41_43 not in x.edges && e9_41 in x.edges) || (e9_11 in x.edges && e11_43 not in x.edges && e41_43 in x.edges && e9_41 not in x.edges)||
(e12_14 not in x.edges && e14_46 in x.edges && e44_46 not in x.edges && e12_44 in x.edges) || (e12_14 in x.edges && e14_46 not in x.edges && e44_46 in x.edges && e12_44 not in x.edges)||
(e13_15 not in x.edges && e15_47 in x.edges && e45_47 not in x.edges && e13_45 in x.edges) || (e13_15 in x.edges && e15_47 not in x.edges && e45_47 in x.edges && e13_45 not in x.edges)||
(e16_18 not in x.edges && e18_50 in x.edges && e48_50 not in x.edges && e16_48 in x.edges) || (e16_18 in x.edges && e18_50 not in x.edges && e48_50 in x.edges && e16_48 not in x.edges)||
(e17_19 not in x.edges && e19_51 in x.edges && e49_51 not in x.edges && e17_49 in x.edges) || (e17_19 in x.edges && e19_51 not in x.edges && e49_51 in x.edges && e17_49 not in x.edges)||
(e20_22 not in x.edges && e22_54 in x.edges && e52_54 not in x.edges && e20_52 in x.edges) || (e20_22 in x.edges && e22_54 not in x.edges && e52_54 in x.edges && e20_52 not in x.edges)||
(e21_23 not in x.edges && e23_55 in x.edges && e53_55 not in x.edges && e21_53 in x.edges) || (e21_23 in x.edges && e23_55 not in x.edges && e53_55 in x.edges && e21_53 not in x.edges)||
(e24_26 not in x.edges && e26_58 in x.edges && e56_58 not in x.edges && e24_56 in x.edges) || (e24_26 in x.edges && e26_58 not in x.edges && e56_58 in x.edges && e24_56 not in x.edges)||
(e25_27 not in x.edges && e27_59 in x.edges && e57_59 not in x.edges && e25_57 in x.edges) || (e25_27 in x.edges && e27_59 not in x.edges && e57_59 in x.edges && e25_57 not in x.edges)||
(e28_30 not in x.edges && e30_62 in x.edges && e60_62 not in x.edges && e28_60 in x.edges) || (e28_30 in x.edges && e30_62 not in x.edges && e60_62 in x.edges && e28_60 not in x.edges)||
(e29_31 not in x.edges && e31_63 in x.edges && e61_63 not in x.edges && e29_61 in x.edges) || (e29_31 in x.edges && e31_63 not in x.edges && e61_63 in x.edges && e29_61 not in x.edges)||
(e0_4 not in x.edges && e4_36 in x.edges && e32_36 not in x.edges && e0_32 in x.edges) || (e0_4 in x.edges && e4_36 not in x.edges && e32_36 in x.edges && e0_32 not in x.edges)||
(e1_5 not in x.edges && e5_37 in x.edges && e33_37 not in x.edges && e1_33 in x.edges) || (e1_5 in x.edges && e5_37 not in x.edges && e33_37 in x.edges && e1_33 not in x.edges)||
(e2_6 not in x.edges && e6_38 in x.edges && e34_38 not in x.edges && e2_34 in x.edges) || (e2_6 in x.edges && e6_38 not in x.edges && e34_38 in x.edges && e2_34 not in x.edges)||
(e3_7 not in x.edges && e7_39 in x.edges && e35_39 not in x.edges && e3_35 in x.edges) || (e3_7 in x.edges && e7_39 not in x.edges && e35_39 in x.edges && e3_35 not in x.edges)||
(e8_12 not in x.edges && e12_44 in x.edges && e40_44 not in x.edges && e8_40 in x.edges) || (e8_12 in x.edges && e12_44 not in x.edges && e40_44 in x.edges && e8_40 not in x.edges)||
(e9_13 not in x.edges && e13_45 in x.edges && e41_45 not in x.edges && e9_41 in x.edges) || (e9_13 in x.edges && e13_45 not in x.edges && e41_45 in x.edges && e9_41 not in x.edges)||
(e10_14 not in x.edges && e14_46 in x.edges && e42_46 not in x.edges && e10_42 in x.edges) || (e10_14 in x.edges && e14_46 not in x.edges && e42_46 in x.edges && e10_42 not in x.edges)||
(e11_15 not in x.edges && e15_47 in x.edges && e43_47 not in x.edges && e11_43 in x.edges) || (e11_15 in x.edges && e15_47 not in x.edges && e43_47 in x.edges && e11_43 not in x.edges)||
(e16_20 not in x.edges && e20_52 in x.edges && e48_52 not in x.edges && e16_48 in x.edges) || (e16_20 in x.edges && e20_52 not in x.edges && e48_52 in x.edges && e16_48 not in x.edges)||
(e17_21 not in x.edges && e21_53 in x.edges && e49_53 not in x.edges && e17_49 in x.edges) || (e17_21 in x.edges && e21_53 not in x.edges && e49_53 in x.edges && e17_49 not in x.edges)||
(e18_22 not in x.edges && e22_54 in x.edges && e50_54 not in x.edges && e18_50 in x.edges) || (e18_22 in x.edges && e22_54 not in x.edges && e50_54 in x.edges && e18_50 not in x.edges)||
(e19_23 not in x.edges && e23_55 in x.edges && e51_55 not in x.edges && e19_51 in x.edges) || (e19_23 in x.edges && e23_55 not in x.edges && e51_55 in x.edges && e19_51 not in x.edges)||
(e24_28 not in x.edges && e28_60 in x.edges && e56_60 not in x.edges && e24_56 in x.edges) || (e24_28 in x.edges && e28_60 not in x.edges && e56_60 in x.edges && e24_56 not in x.edges)||
(e25_29 not in x.edges && e29_61 in x.edges && e57_61 not in x.edges && e25_57 in x.edges) || (e25_29 in x.edges && e29_61 not in x.edges && e57_61 in x.edges && e25_57 not in x.edges)||
(e26_30 not in x.edges && e30_62 in x.edges && e58_62 not in x.edges && e26_58 in x.edges) || (e26_30 in x.edges && e30_62 not in x.edges && e58_62 in x.edges && e26_58 not in x.edges)||
(e27_31 not in x.edges && e31_63 in x.edges && e59_63 not in x.edges && e27_59 in x.edges) || (e27_31 in x.edges && e31_63 not in x.edges && e59_63 in x.edges && e27_59 not in x.edges)||
(e0_8 not in x.edges && e8_40 in x.edges && e32_40 not in x.edges && e0_32 in x.edges) || (e0_8 in x.edges && e8_40 not in x.edges && e32_40 in x.edges && e0_32 not in x.edges)||
(e1_9 not in x.edges && e9_41 in x.edges && e33_41 not in x.edges && e1_33 in x.edges) || (e1_9 in x.edges && e9_41 not in x.edges && e33_41 in x.edges && e1_33 not in x.edges)||
(e2_10 not in x.edges && e10_42 in x.edges && e34_42 not in x.edges && e2_34 in x.edges) || (e2_10 in x.edges && e10_42 not in x.edges && e34_42 in x.edges && e2_34 not in x.edges)||
(e3_11 not in x.edges && e11_43 in x.edges && e35_43 not in x.edges && e3_35 in x.edges) || (e3_11 in x.edges && e11_43 not in x.edges && e35_43 in x.edges && e3_35 not in x.edges)||
(e4_12 not in x.edges && e12_44 in x.edges && e36_44 not in x.edges && e4_36 in x.edges) || (e4_12 in x.edges && e12_44 not in x.edges && e36_44 in x.edges && e4_36 not in x.edges)||
(e5_13 not in x.edges && e13_45 in x.edges && e37_45 not in x.edges && e5_37 in x.edges) || (e5_13 in x.edges && e13_45 not in x.edges && e37_45 in x.edges && e5_37 not in x.edges)||
(e6_14 not in x.edges && e14_46 in x.edges && e38_46 not in x.edges && e6_38 in x.edges) || (e6_14 in x.edges && e14_46 not in x.edges && e38_46 in x.edges && e6_38 not in x.edges)||
(e7_15 not in x.edges && e15_47 in x.edges && e39_47 not in x.edges && e7_39 in x.edges) || (e7_15 in x.edges && e15_47 not in x.edges && e39_47 in x.edges && e7_39 not in x.edges)||
(e16_24 not in x.edges && e24_56 in x.edges && e48_56 not in x.edges && e16_48 in x.edges) || (e16_24 in x.edges && e24_56 not in x.edges && e48_56 in x.edges && e16_48 not in x.edges)||
(e17_25 not in x.edges && e25_57 in x.edges && e49_57 not in x.edges && e17_49 in x.edges) || (e17_25 in x.edges && e25_57 not in x.edges && e49_57 in x.edges && e17_49 not in x.edges)||
(e18_26 not in x.edges && e26_58 in x.edges && e50_58 not in x.edges && e18_50 in x.edges) || (e18_26 in x.edges && e26_58 not in x.edges && e50_58 in x.edges && e18_50 not in x.edges)||
(e19_27 not in x.edges && e27_59 in x.edges && e51_59 not in x.edges && e19_51 in x.edges) || (e19_27 in x.edges && e27_59 not in x.edges && e51_59 in x.edges && e19_51 not in x.edges)||
(e20_28 not in x.edges && e28_60 in x.edges && e52_60 not in x.edges && e20_52 in x.edges) || (e20_28 in x.edges && e28_60 not in x.edges && e52_60 in x.edges && e20_52 not in x.edges)||
(e21_29 not in x.edges && e29_61 in x.edges && e53_61 not in x.edges && e21_53 in x.edges) || (e21_29 in x.edges && e29_61 not in x.edges && e53_61 in x.edges && e21_53 not in x.edges)||
(e22_30 not in x.edges && e30_62 in x.edges && e54_62 not in x.edges && e22_54 in x.edges) || (e22_30 in x.edges && e30_62 not in x.edges && e54_62 in x.edges && e22_54 not in x.edges)||
(e23_31 not in x.edges && e31_63 in x.edges && e55_63 not in x.edges && e23_55 in x.edges) || (e23_31 in x.edges && e31_63 not in x.edges && e55_63 in x.edges && e23_55 not in x.edges)||
(e0_16 not in x.edges && e16_48 in x.edges && e32_48 not in x.edges && e0_32 in x.edges) || (e0_16 in x.edges && e16_48 not in x.edges && e32_48 in x.edges && e0_32 not in x.edges)||
(e1_17 not in x.edges && e17_49 in x.edges && e33_49 not in x.edges && e1_33 in x.edges) || (e1_17 in x.edges && e17_49 not in x.edges && e33_49 in x.edges && e1_33 not in x.edges)||
(e2_18 not in x.edges && e18_50 in x.edges && e34_50 not in x.edges && e2_34 in x.edges) || (e2_18 in x.edges && e18_50 not in x.edges && e34_50 in x.edges && e2_34 not in x.edges)||
(e3_19 not in x.edges && e19_51 in x.edges && e35_51 not in x.edges && e3_35 in x.edges) || (e3_19 in x.edges && e19_51 not in x.edges && e35_51 in x.edges && e3_35 not in x.edges)||
(e4_20 not in x.edges && e20_52 in x.edges && e36_52 not in x.edges && e4_36 in x.edges) || (e4_20 in x.edges && e20_52 not in x.edges && e36_52 in x.edges && e4_36 not in x.edges)||
(e5_21 not in x.edges && e21_53 in x.edges && e37_53 not in x.edges && e5_37 in x.edges) || (e5_21 in x.edges && e21_53 not in x.edges && e37_53 in x.edges && e5_37 not in x.edges)||
(e6_22 not in x.edges && e22_54 in x.edges && e38_54 not in x.edges && e6_38 in x.edges) || (e6_22 in x.edges && e22_54 not in x.edges && e38_54 in x.edges && e6_38 not in x.edges)||
(e7_23 not in x.edges && e23_55 in x.edges && e39_55 not in x.edges && e7_39 in x.edges) || (e7_23 in x.edges && e23_55 not in x.edges && e39_55 in x.edges && e7_39 not in x.edges)||
(e8_24 not in x.edges && e24_56 in x.edges && e40_56 not in x.edges && e8_40 in x.edges) || (e8_24 in x.edges && e24_56 not in x.edges && e40_56 in x.edges && e8_40 not in x.edges)||
(e9_25 not in x.edges && e25_57 in x.edges && e41_57 not in x.edges && e9_41 in x.edges) || (e9_25 in x.edges && e25_57 not in x.edges && e41_57 in x.edges && e9_41 not in x.edges)||
(e10_26 not in x.edges && e26_58 in x.edges && e42_58 not in x.edges && e10_42 in x.edges) || (e10_26 in x.edges && e26_58 not in x.edges && e42_58 in x.edges && e10_42 not in x.edges)||
(e11_27 not in x.edges && e27_59 in x.edges && e43_59 not in x.edges && e11_43 in x.edges) || (e11_27 in x.edges && e27_59 not in x.edges && e43_59 in x.edges && e11_43 not in x.edges)||
(e12_28 not in x.edges && e28_60 in x.edges && e44_60 not in x.edges && e12_44 in x.edges) || (e12_28 in x.edges && e28_60 not in x.edges && e44_60 in x.edges && e12_44 not in x.edges)||
(e13_29 not in x.edges && e29_61 in x.edges && e45_61 not in x.edges && e13_45 in x.edges) || (e13_29 in x.edges && e29_61 not in x.edges && e45_61 in x.edges && e13_45 not in x.edges)||
(e14_30 not in x.edges && e30_62 in x.edges && e46_62 not in x.edges && e14_46 in x.edges) || (e14_30 in x.edges && e30_62 not in x.edges && e46_62 in x.edges && e14_46 not in x.edges)||
(e15_31 not in x.edges && e31_63 in x.edges && e47_63 not in x.edges && e15_47 in x.edges) || (e15_31 in x.edges && e31_63 not in x.edges && e47_63 in x.edges && e15_47 not in x.edges)
}

pred antipodal_connected[x:Graph]{
connected[n0, n63, x] ||
connected[n1, n62, x] ||
connected[n2, n61, x] ||
connected[n3, n60, x] ||
connected[n4, n59, x] ||
connected[n5, n58, x] ||
connected[n6, n57, x] ||
connected[n7, n56, x] ||
connected[n8, n55, x] ||
connected[n9, n54, x] ||
connected[n10, n53, x] ||
connected[n11, n52, x] ||
connected[n12, n51, x] ||
connected[n13, n50, x] ||
connected[n14, n49, x] ||
connected[n15, n48, x] ||
connected[n16, n47, x] ||
connected[n17, n46, x] ||
connected[n18, n45, x] ||
connected[n19, n44, x] ||
connected[n20, n43, x] ||
connected[n21, n42, x] ||
connected[n22, n41, x] ||
connected[n23, n40, x] ||
connected[n24, n39, x] ||
connected[n25, n38, x] ||
connected[n26, n37, x] ||
connected[n27, n36, x] ||
connected[n28, n35, x] ||
connected[n29, n34, x] ||
connected[n30, n33, x] ||
connected[n31, n32, x]
}

one sig n0 extends Node { }
one sig n1 extends Node { }
one sig n2 extends Node { }
one sig n3 extends Node { }
one sig n4 extends Node { }
one sig n5 extends Node { }
one sig n6 extends Node { }
one sig n7 extends Node { }
one sig n8 extends Node { }
one sig n9 extends Node { }
one sig n10 extends Node { }
one sig n11 extends Node { }
one sig n12 extends Node { }
one sig n13 extends Node { }
one sig n14 extends Node { }
one sig n15 extends Node { }
one sig n16 extends Node { }
one sig n17 extends Node { }
one sig n18 extends Node { }
one sig n19 extends Node { }
one sig n20 extends Node { }
one sig n21 extends Node { }
one sig n22 extends Node { }
one sig n23 extends Node { }
one sig n24 extends Node { }
one sig n25 extends Node { }
one sig n26 extends Node { }
one sig n27 extends Node { }
one sig n28 extends Node { }
one sig n29 extends Node { }
one sig n30 extends Node { }
one sig n31 extends Node { }
one sig n32 extends Node { }
one sig n33 extends Node { }
one sig n34 extends Node { }
one sig n35 extends Node { }
one sig n36 extends Node { }
one sig n37 extends Node { }
one sig n38 extends Node { }
one sig n39 extends Node { }
one sig n40 extends Node { }
one sig n41 extends Node { }
one sig n42 extends Node { }
one sig n43 extends Node { }
one sig n44 extends Node { }
one sig n45 extends Node { }
one sig n46 extends Node { }
one sig n47 extends Node { }
one sig n48 extends Node { }
one sig n49 extends Node { }
one sig n50 extends Node { }
one sig n51 extends Node { }
one sig n52 extends Node { }
one sig n53 extends Node { }
one sig n54 extends Node { }
one sig n55 extends Node { }
one sig n56 extends Node { }
one sig n57 extends Node { }
one sig n58 extends Node { }
one sig n59 extends Node { }
one sig n60 extends Node { }
one sig n61 extends Node { }
one sig n62 extends Node { }
one sig n63 extends Node { }

one sig e0_1 extends Edge { } {
  src + dst = n0 + n1 }
one sig e1_0 extends Edge { } {
  src + dst = n0 + n1 }
one sig e0_2 extends Edge { } {
  src + dst = n0 + n2 }
one sig e2_0 extends Edge { } {
  src + dst = n0 + n2 }
one sig e0_4 extends Edge { } {
  src + dst = n0 + n4 }
one sig e4_0 extends Edge { } {
  src + dst = n0 + n4 }
one sig e0_8 extends Edge { } {
  src + dst = n0 + n8 }
one sig e8_0 extends Edge { } {
  src + dst = n0 + n8 }
one sig e0_16 extends Edge { } {
  src + dst = n0 + n16 }
one sig e16_0 extends Edge { } {
  src + dst = n0 + n16 }
one sig e0_32 extends Edge { } {
  src + dst = n0 + n32 }
one sig e32_0 extends Edge { } {
  src + dst = n0 + n32 }
one sig e1_3 extends Edge { } {
  src + dst = n1 + n3 }
one sig e3_1 extends Edge { } {
  src + dst = n1 + n3 }
one sig e1_5 extends Edge { } {
  src + dst = n1 + n5 }
one sig e5_1 extends Edge { } {
  src + dst = n1 + n5 }
one sig e1_9 extends Edge { } {
  src + dst = n1 + n9 }
one sig e9_1 extends Edge { } {
  src + dst = n1 + n9 }
one sig e1_17 extends Edge { } {
  src + dst = n1 + n17 }
one sig e17_1 extends Edge { } {
  src + dst = n1 + n17 }
one sig e1_33 extends Edge { } {
  src + dst = n1 + n33 }
one sig e33_1 extends Edge { } {
  src + dst = n1 + n33 }
one sig e2_3 extends Edge { } {
  src + dst = n2 + n3 }
one sig e3_2 extends Edge { } {
  src + dst = n2 + n3 }
one sig e2_6 extends Edge { } {
  src + dst = n2 + n6 }
one sig e6_2 extends Edge { } {
  src + dst = n2 + n6 }
one sig e2_10 extends Edge { } {
  src + dst = n2 + n10 }
one sig e10_2 extends Edge { } {
  src + dst = n2 + n10 }
one sig e2_18 extends Edge { } {
  src + dst = n2 + n18 }
one sig e18_2 extends Edge { } {
  src + dst = n2 + n18 }
one sig e2_34 extends Edge { } {
  src + dst = n2 + n34 }
one sig e34_2 extends Edge { } {
  src + dst = n2 + n34 }
one sig e3_7 extends Edge { } {
  src + dst = n3 + n7 }
one sig e7_3 extends Edge { } {
  src + dst = n3 + n7 }
one sig e3_11 extends Edge { } {
  src + dst = n3 + n11 }
one sig e11_3 extends Edge { } {
  src + dst = n3 + n11 }
one sig e3_19 extends Edge { } {
  src + dst = n3 + n19 }
one sig e19_3 extends Edge { } {
  src + dst = n3 + n19 }
one sig e3_35 extends Edge { } {
  src + dst = n3 + n35 }
one sig e35_3 extends Edge { } {
  src + dst = n3 + n35 }
one sig e4_5 extends Edge { } {
  src + dst = n4 + n5 }
one sig e5_4 extends Edge { } {
  src + dst = n4 + n5 }
one sig e4_6 extends Edge { } {
  src + dst = n4 + n6 }
one sig e6_4 extends Edge { } {
  src + dst = n4 + n6 }
one sig e4_12 extends Edge { } {
  src + dst = n4 + n12 }
one sig e12_4 extends Edge { } {
  src + dst = n4 + n12 }
one sig e4_20 extends Edge { } {
  src + dst = n4 + n20 }
one sig e20_4 extends Edge { } {
  src + dst = n4 + n20 }
one sig e4_36 extends Edge { } {
  src + dst = n4 + n36 }
one sig e36_4 extends Edge { } {
  src + dst = n4 + n36 }
one sig e5_7 extends Edge { } {
  src + dst = n5 + n7 }
one sig e7_5 extends Edge { } {
  src + dst = n5 + n7 }
one sig e5_13 extends Edge { } {
  src + dst = n5 + n13 }
one sig e13_5 extends Edge { } {
  src + dst = n5 + n13 }
one sig e5_21 extends Edge { } {
  src + dst = n5 + n21 }
one sig e21_5 extends Edge { } {
  src + dst = n5 + n21 }
one sig e5_37 extends Edge { } {
  src + dst = n5 + n37 }
one sig e37_5 extends Edge { } {
  src + dst = n5 + n37 }
one sig e6_7 extends Edge { } {
  src + dst = n6 + n7 }
one sig e7_6 extends Edge { } {
  src + dst = n6 + n7 }
one sig e6_14 extends Edge { } {
  src + dst = n6 + n14 }
one sig e14_6 extends Edge { } {
  src + dst = n6 + n14 }
one sig e6_22 extends Edge { } {
  src + dst = n6 + n22 }
one sig e22_6 extends Edge { } {
  src + dst = n6 + n22 }
one sig e6_38 extends Edge { } {
  src + dst = n6 + n38 }
one sig e38_6 extends Edge { } {
  src + dst = n6 + n38 }
one sig e7_15 extends Edge { } {
  src + dst = n7 + n15 }
one sig e15_7 extends Edge { } {
  src + dst = n7 + n15 }
one sig e7_23 extends Edge { } {
  src + dst = n7 + n23 }
one sig e23_7 extends Edge { } {
  src + dst = n7 + n23 }
one sig e7_39 extends Edge { } {
  src + dst = n7 + n39 }
one sig e39_7 extends Edge { } {
  src + dst = n7 + n39 }
one sig e8_9 extends Edge { } {
  src + dst = n8 + n9 }
one sig e9_8 extends Edge { } {
  src + dst = n8 + n9 }
one sig e8_10 extends Edge { } {
  src + dst = n8 + n10 }
one sig e10_8 extends Edge { } {
  src + dst = n8 + n10 }
one sig e8_12 extends Edge { } {
  src + dst = n8 + n12 }
one sig e12_8 extends Edge { } {
  src + dst = n8 + n12 }
one sig e8_24 extends Edge { } {
  src + dst = n8 + n24 }
one sig e24_8 extends Edge { } {
  src + dst = n8 + n24 }
one sig e8_40 extends Edge { } {
  src + dst = n8 + n40 }
one sig e40_8 extends Edge { } {
  src + dst = n8 + n40 }
one sig e9_11 extends Edge { } {
  src + dst = n9 + n11 }
one sig e11_9 extends Edge { } {
  src + dst = n9 + n11 }
one sig e9_13 extends Edge { } {
  src + dst = n9 + n13 }
one sig e13_9 extends Edge { } {
  src + dst = n9 + n13 }
one sig e9_25 extends Edge { } {
  src + dst = n9 + n25 }
one sig e25_9 extends Edge { } {
  src + dst = n9 + n25 }
one sig e9_41 extends Edge { } {
  src + dst = n9 + n41 }
one sig e41_9 extends Edge { } {
  src + dst = n9 + n41 }
one sig e10_11 extends Edge { } {
  src + dst = n10 + n11 }
one sig e11_10 extends Edge { } {
  src + dst = n10 + n11 }
one sig e10_14 extends Edge { } {
  src + dst = n10 + n14 }
one sig e14_10 extends Edge { } {
  src + dst = n10 + n14 }
one sig e10_26 extends Edge { } {
  src + dst = n10 + n26 }
one sig e26_10 extends Edge { } {
  src + dst = n10 + n26 }
one sig e10_42 extends Edge { } {
  src + dst = n10 + n42 }
one sig e42_10 extends Edge { } {
  src + dst = n10 + n42 }
one sig e11_15 extends Edge { } {
  src + dst = n11 + n15 }
one sig e15_11 extends Edge { } {
  src + dst = n11 + n15 }
one sig e11_27 extends Edge { } {
  src + dst = n11 + n27 }
one sig e27_11 extends Edge { } {
  src + dst = n11 + n27 }
one sig e11_43 extends Edge { } {
  src + dst = n11 + n43 }
one sig e43_11 extends Edge { } {
  src + dst = n11 + n43 }
one sig e12_13 extends Edge { } {
  src + dst = n12 + n13 }
one sig e13_12 extends Edge { } {
  src + dst = n12 + n13 }
one sig e12_14 extends Edge { } {
  src + dst = n12 + n14 }
one sig e14_12 extends Edge { } {
  src + dst = n12 + n14 }
one sig e12_28 extends Edge { } {
  src + dst = n12 + n28 }
one sig e28_12 extends Edge { } {
  src + dst = n12 + n28 }
one sig e12_44 extends Edge { } {
  src + dst = n12 + n44 }
one sig e44_12 extends Edge { } {
  src + dst = n12 + n44 }
one sig e13_15 extends Edge { } {
  src + dst = n13 + n15 }
one sig e15_13 extends Edge { } {
  src + dst = n13 + n15 }
one sig e13_29 extends Edge { } {
  src + dst = n13 + n29 }
one sig e29_13 extends Edge { } {
  src + dst = n13 + n29 }
one sig e13_45 extends Edge { } {
  src + dst = n13 + n45 }
one sig e45_13 extends Edge { } {
  src + dst = n13 + n45 }
one sig e14_15 extends Edge { } {
  src + dst = n14 + n15 }
one sig e15_14 extends Edge { } {
  src + dst = n14 + n15 }
one sig e14_30 extends Edge { } {
  src + dst = n14 + n30 }
one sig e30_14 extends Edge { } {
  src + dst = n14 + n30 }
one sig e14_46 extends Edge { } {
  src + dst = n14 + n46 }
one sig e46_14 extends Edge { } {
  src + dst = n14 + n46 }
one sig e15_31 extends Edge { } {
  src + dst = n15 + n31 }
one sig e31_15 extends Edge { } {
  src + dst = n15 + n31 }
one sig e15_47 extends Edge { } {
  src + dst = n15 + n47 }
one sig e47_15 extends Edge { } {
  src + dst = n15 + n47 }
one sig e16_17 extends Edge { } {
  src + dst = n16 + n17 }
one sig e17_16 extends Edge { } {
  src + dst = n16 + n17 }
one sig e16_18 extends Edge { } {
  src + dst = n16 + n18 }
one sig e18_16 extends Edge { } {
  src + dst = n16 + n18 }
one sig e16_20 extends Edge { } {
  src + dst = n16 + n20 }
one sig e20_16 extends Edge { } {
  src + dst = n16 + n20 }
one sig e16_24 extends Edge { } {
  src + dst = n16 + n24 }
one sig e24_16 extends Edge { } {
  src + dst = n16 + n24 }
one sig e16_48 extends Edge { } {
  src + dst = n16 + n48 }
one sig e48_16 extends Edge { } {
  src + dst = n16 + n48 }
one sig e17_19 extends Edge { } {
  src + dst = n17 + n19 }
one sig e19_17 extends Edge { } {
  src + dst = n17 + n19 }
one sig e17_21 extends Edge { } {
  src + dst = n17 + n21 }
one sig e21_17 extends Edge { } {
  src + dst = n17 + n21 }
one sig e17_25 extends Edge { } {
  src + dst = n17 + n25 }
one sig e25_17 extends Edge { } {
  src + dst = n17 + n25 }
one sig e17_49 extends Edge { } {
  src + dst = n17 + n49 }
one sig e49_17 extends Edge { } {
  src + dst = n17 + n49 }
one sig e18_19 extends Edge { } {
  src + dst = n18 + n19 }
one sig e19_18 extends Edge { } {
  src + dst = n18 + n19 }
one sig e18_22 extends Edge { } {
  src + dst = n18 + n22 }
one sig e22_18 extends Edge { } {
  src + dst = n18 + n22 }
one sig e18_26 extends Edge { } {
  src + dst = n18 + n26 }
one sig e26_18 extends Edge { } {
  src + dst = n18 + n26 }
one sig e18_50 extends Edge { } {
  src + dst = n18 + n50 }
one sig e50_18 extends Edge { } {
  src + dst = n18 + n50 }
one sig e19_23 extends Edge { } {
  src + dst = n19 + n23 }
one sig e23_19 extends Edge { } {
  src + dst = n19 + n23 }
one sig e19_27 extends Edge { } {
  src + dst = n19 + n27 }
one sig e27_19 extends Edge { } {
  src + dst = n19 + n27 }
one sig e19_51 extends Edge { } {
  src + dst = n19 + n51 }
one sig e51_19 extends Edge { } {
  src + dst = n19 + n51 }
one sig e20_21 extends Edge { } {
  src + dst = n20 + n21 }
one sig e21_20 extends Edge { } {
  src + dst = n20 + n21 }
one sig e20_22 extends Edge { } {
  src + dst = n20 + n22 }
one sig e22_20 extends Edge { } {
  src + dst = n20 + n22 }
one sig e20_28 extends Edge { } {
  src + dst = n20 + n28 }
one sig e28_20 extends Edge { } {
  src + dst = n20 + n28 }
one sig e20_52 extends Edge { } {
  src + dst = n20 + n52 }
one sig e52_20 extends Edge { } {
  src + dst = n20 + n52 }
one sig e21_23 extends Edge { } {
  src + dst = n21 + n23 }
one sig e23_21 extends Edge { } {
  src + dst = n21 + n23 }
one sig e21_29 extends Edge { } {
  src + dst = n21 + n29 }
one sig e29_21 extends Edge { } {
  src + dst = n21 + n29 }
one sig e21_53 extends Edge { } {
  src + dst = n21 + n53 }
one sig e53_21 extends Edge { } {
  src + dst = n21 + n53 }
one sig e22_23 extends Edge { } {
  src + dst = n22 + n23 }
one sig e23_22 extends Edge { } {
  src + dst = n22 + n23 }
one sig e22_30 extends Edge { } {
  src + dst = n22 + n30 }
one sig e30_22 extends Edge { } {
  src + dst = n22 + n30 }
one sig e22_54 extends Edge { } {
  src + dst = n22 + n54 }
one sig e54_22 extends Edge { } {
  src + dst = n22 + n54 }
one sig e23_31 extends Edge { } {
  src + dst = n23 + n31 }
one sig e31_23 extends Edge { } {
  src + dst = n23 + n31 }
one sig e23_55 extends Edge { } {
  src + dst = n23 + n55 }
one sig e55_23 extends Edge { } {
  src + dst = n23 + n55 }
one sig e24_25 extends Edge { } {
  src + dst = n24 + n25 }
one sig e25_24 extends Edge { } {
  src + dst = n24 + n25 }
one sig e24_26 extends Edge { } {
  src + dst = n24 + n26 }
one sig e26_24 extends Edge { } {
  src + dst = n24 + n26 }
one sig e24_28 extends Edge { } {
  src + dst = n24 + n28 }
one sig e28_24 extends Edge { } {
  src + dst = n24 + n28 }
one sig e24_56 extends Edge { } {
  src + dst = n24 + n56 }
one sig e56_24 extends Edge { } {
  src + dst = n24 + n56 }
one sig e25_27 extends Edge { } {
  src + dst = n25 + n27 }
one sig e27_25 extends Edge { } {
  src + dst = n25 + n27 }
one sig e25_29 extends Edge { } {
  src + dst = n25 + n29 }
one sig e29_25 extends Edge { } {
  src + dst = n25 + n29 }
one sig e25_57 extends Edge { } {
  src + dst = n25 + n57 }
one sig e57_25 extends Edge { } {
  src + dst = n25 + n57 }
one sig e26_27 extends Edge { } {
  src + dst = n26 + n27 }
one sig e27_26 extends Edge { } {
  src + dst = n26 + n27 }
one sig e26_30 extends Edge { } {
  src + dst = n26 + n30 }
one sig e30_26 extends Edge { } {
  src + dst = n26 + n30 }
one sig e26_58 extends Edge { } {
  src + dst = n26 + n58 }
one sig e58_26 extends Edge { } {
  src + dst = n26 + n58 }
one sig e27_31 extends Edge { } {
  src + dst = n27 + n31 }
one sig e31_27 extends Edge { } {
  src + dst = n27 + n31 }
one sig e27_59 extends Edge { } {
  src + dst = n27 + n59 }
one sig e59_27 extends Edge { } {
  src + dst = n27 + n59 }
one sig e28_29 extends Edge { } {
  src + dst = n28 + n29 }
one sig e29_28 extends Edge { } {
  src + dst = n28 + n29 }
one sig e28_30 extends Edge { } {
  src + dst = n28 + n30 }
one sig e30_28 extends Edge { } {
  src + dst = n28 + n30 }
one sig e28_60 extends Edge { } {
  src + dst = n28 + n60 }
one sig e60_28 extends Edge { } {
  src + dst = n28 + n60 }
one sig e29_31 extends Edge { } {
  src + dst = n29 + n31 }
one sig e31_29 extends Edge { } {
  src + dst = n29 + n31 }
one sig e29_61 extends Edge { } {
  src + dst = n29 + n61 }
one sig e61_29 extends Edge { } {
  src + dst = n29 + n61 }
one sig e30_31 extends Edge { } {
  src + dst = n30 + n31 }
one sig e31_30 extends Edge { } {
  src + dst = n30 + n31 }
one sig e30_62 extends Edge { } {
  src + dst = n30 + n62 }
one sig e62_30 extends Edge { } {
  src + dst = n30 + n62 }
one sig e31_63 extends Edge { } {
  src + dst = n31 + n63 }
one sig e63_31 extends Edge { } {
  src + dst = n31 + n63 }
one sig e32_33 extends Edge { } {
  src + dst = n32 + n33 }
one sig e33_32 extends Edge { } {
  src + dst = n32 + n33 }
one sig e32_34 extends Edge { } {
  src + dst = n32 + n34 }
one sig e34_32 extends Edge { } {
  src + dst = n32 + n34 }
one sig e32_36 extends Edge { } {
  src + dst = n32 + n36 }
one sig e36_32 extends Edge { } {
  src + dst = n32 + n36 }
one sig e32_40 extends Edge { } {
  src + dst = n32 + n40 }
one sig e40_32 extends Edge { } {
  src + dst = n32 + n40 }
one sig e32_48 extends Edge { } {
  src + dst = n32 + n48 }
one sig e48_32 extends Edge { } {
  src + dst = n32 + n48 }
one sig e33_35 extends Edge { } {
  src + dst = n33 + n35 }
one sig e35_33 extends Edge { } {
  src + dst = n33 + n35 }
one sig e33_37 extends Edge { } {
  src + dst = n33 + n37 }
one sig e37_33 extends Edge { } {
  src + dst = n33 + n37 }
one sig e33_41 extends Edge { } {
  src + dst = n33 + n41 }
one sig e41_33 extends Edge { } {
  src + dst = n33 + n41 }
one sig e33_49 extends Edge { } {
  src + dst = n33 + n49 }
one sig e49_33 extends Edge { } {
  src + dst = n33 + n49 }
one sig e34_35 extends Edge { } {
  src + dst = n34 + n35 }
one sig e35_34 extends Edge { } {
  src + dst = n34 + n35 }
one sig e34_38 extends Edge { } {
  src + dst = n34 + n38 }
one sig e38_34 extends Edge { } {
  src + dst = n34 + n38 }
one sig e34_42 extends Edge { } {
  src + dst = n34 + n42 }
one sig e42_34 extends Edge { } {
  src + dst = n34 + n42 }
one sig e34_50 extends Edge { } {
  src + dst = n34 + n50 }
one sig e50_34 extends Edge { } {
  src + dst = n34 + n50 }
one sig e35_39 extends Edge { } {
  src + dst = n35 + n39 }
one sig e39_35 extends Edge { } {
  src + dst = n35 + n39 }
one sig e35_43 extends Edge { } {
  src + dst = n35 + n43 }
one sig e43_35 extends Edge { } {
  src + dst = n35 + n43 }
one sig e35_51 extends Edge { } {
  src + dst = n35 + n51 }
one sig e51_35 extends Edge { } {
  src + dst = n35 + n51 }
one sig e36_37 extends Edge { } {
  src + dst = n36 + n37 }
one sig e37_36 extends Edge { } {
  src + dst = n36 + n37 }
one sig e36_38 extends Edge { } {
  src + dst = n36 + n38 }
one sig e38_36 extends Edge { } {
  src + dst = n36 + n38 }
one sig e36_44 extends Edge { } {
  src + dst = n36 + n44 }
one sig e44_36 extends Edge { } {
  src + dst = n36 + n44 }
one sig e36_52 extends Edge { } {
  src + dst = n36 + n52 }
one sig e52_36 extends Edge { } {
  src + dst = n36 + n52 }
one sig e37_39 extends Edge { } {
  src + dst = n37 + n39 }
one sig e39_37 extends Edge { } {
  src + dst = n37 + n39 }
one sig e37_45 extends Edge { } {
  src + dst = n37 + n45 }
one sig e45_37 extends Edge { } {
  src + dst = n37 + n45 }
one sig e37_53 extends Edge { } {
  src + dst = n37 + n53 }
one sig e53_37 extends Edge { } {
  src + dst = n37 + n53 }
one sig e38_39 extends Edge { } {
  src + dst = n38 + n39 }
one sig e39_38 extends Edge { } {
  src + dst = n38 + n39 }
one sig e38_46 extends Edge { } {
  src + dst = n38 + n46 }
one sig e46_38 extends Edge { } {
  src + dst = n38 + n46 }
one sig e38_54 extends Edge { } {
  src + dst = n38 + n54 }
one sig e54_38 extends Edge { } {
  src + dst = n38 + n54 }
one sig e39_47 extends Edge { } {
  src + dst = n39 + n47 }
one sig e47_39 extends Edge { } {
  src + dst = n39 + n47 }
one sig e39_55 extends Edge { } {
  src + dst = n39 + n55 }
one sig e55_39 extends Edge { } {
  src + dst = n39 + n55 }
one sig e40_41 extends Edge { } {
  src + dst = n40 + n41 }
one sig e41_40 extends Edge { } {
  src + dst = n40 + n41 }
one sig e40_42 extends Edge { } {
  src + dst = n40 + n42 }
one sig e42_40 extends Edge { } {
  src + dst = n40 + n42 }
one sig e40_44 extends Edge { } {
  src + dst = n40 + n44 }
one sig e44_40 extends Edge { } {
  src + dst = n40 + n44 }
one sig e40_56 extends Edge { } {
  src + dst = n40 + n56 }
one sig e56_40 extends Edge { } {
  src + dst = n40 + n56 }
one sig e41_43 extends Edge { } {
  src + dst = n41 + n43 }
one sig e43_41 extends Edge { } {
  src + dst = n41 + n43 }
one sig e41_45 extends Edge { } {
  src + dst = n41 + n45 }
one sig e45_41 extends Edge { } {
  src + dst = n41 + n45 }
one sig e41_57 extends Edge { } {
  src + dst = n41 + n57 }
one sig e57_41 extends Edge { } {
  src + dst = n41 + n57 }
one sig e42_43 extends Edge { } {
  src + dst = n42 + n43 }
one sig e43_42 extends Edge { } {
  src + dst = n42 + n43 }
one sig e42_46 extends Edge { } {
  src + dst = n42 + n46 }
one sig e46_42 extends Edge { } {
  src + dst = n42 + n46 }
one sig e42_58 extends Edge { } {
  src + dst = n42 + n58 }
one sig e58_42 extends Edge { } {
  src + dst = n42 + n58 }
one sig e43_47 extends Edge { } {
  src + dst = n43 + n47 }
one sig e47_43 extends Edge { } {
  src + dst = n43 + n47 }
one sig e43_59 extends Edge { } {
  src + dst = n43 + n59 }
one sig e59_43 extends Edge { } {
  src + dst = n43 + n59 }
one sig e44_45 extends Edge { } {
  src + dst = n44 + n45 }
one sig e45_44 extends Edge { } {
  src + dst = n44 + n45 }
one sig e44_46 extends Edge { } {
  src + dst = n44 + n46 }
one sig e46_44 extends Edge { } {
  src + dst = n44 + n46 }
one sig e44_60 extends Edge { } {
  src + dst = n44 + n60 }
one sig e60_44 extends Edge { } {
  src + dst = n44 + n60 }
one sig e45_47 extends Edge { } {
  src + dst = n45 + n47 }
one sig e47_45 extends Edge { } {
  src + dst = n45 + n47 }
one sig e45_61 extends Edge { } {
  src + dst = n45 + n61 }
one sig e61_45 extends Edge { } {
  src + dst = n45 + n61 }
one sig e46_47 extends Edge { } {
  src + dst = n46 + n47 }
one sig e47_46 extends Edge { } {
  src + dst = n46 + n47 }
one sig e46_62 extends Edge { } {
  src + dst = n46 + n62 }
one sig e62_46 extends Edge { } {
  src + dst = n46 + n62 }
one sig e47_63 extends Edge { } {
  src + dst = n47 + n63 }
one sig e63_47 extends Edge { } {
  src + dst = n47 + n63 }
one sig e48_49 extends Edge { } {
  src + dst = n48 + n49 }
one sig e49_48 extends Edge { } {
  src + dst = n48 + n49 }
one sig e48_50 extends Edge { } {
  src + dst = n48 + n50 }
one sig e50_48 extends Edge { } {
  src + dst = n48 + n50 }
one sig e48_52 extends Edge { } {
  src + dst = n48 + n52 }
one sig e52_48 extends Edge { } {
  src + dst = n48 + n52 }
one sig e48_56 extends Edge { } {
  src + dst = n48 + n56 }
one sig e56_48 extends Edge { } {
  src + dst = n48 + n56 }
one sig e49_51 extends Edge { } {
  src + dst = n49 + n51 }
one sig e51_49 extends Edge { } {
  src + dst = n49 + n51 }
one sig e49_53 extends Edge { } {
  src + dst = n49 + n53 }
one sig e53_49 extends Edge { } {
  src + dst = n49 + n53 }
one sig e49_57 extends Edge { } {
  src + dst = n49 + n57 }
one sig e57_49 extends Edge { } {
  src + dst = n49 + n57 }
one sig e50_51 extends Edge { } {
  src + dst = n50 + n51 }
one sig e51_50 extends Edge { } {
  src + dst = n50 + n51 }
one sig e50_54 extends Edge { } {
  src + dst = n50 + n54 }
one sig e54_50 extends Edge { } {
  src + dst = n50 + n54 }
one sig e50_58 extends Edge { } {
  src + dst = n50 + n58 }
one sig e58_50 extends Edge { } {
  src + dst = n50 + n58 }
one sig e51_55 extends Edge { } {
  src + dst = n51 + n55 }
one sig e55_51 extends Edge { } {
  src + dst = n51 + n55 }
one sig e51_59 extends Edge { } {
  src + dst = n51 + n59 }
one sig e59_51 extends Edge { } {
  src + dst = n51 + n59 }
one sig e52_53 extends Edge { } {
  src + dst = n52 + n53 }
one sig e53_52 extends Edge { } {
  src + dst = n52 + n53 }
one sig e52_54 extends Edge { } {
  src + dst = n52 + n54 }
one sig e54_52 extends Edge { } {
  src + dst = n52 + n54 }
one sig e52_60 extends Edge { } {
  src + dst = n52 + n60 }
one sig e60_52 extends Edge { } {
  src + dst = n52 + n60 }
one sig e53_55 extends Edge { } {
  src + dst = n53 + n55 }
one sig e55_53 extends Edge { } {
  src + dst = n53 + n55 }
one sig e53_61 extends Edge { } {
  src + dst = n53 + n61 }
one sig e61_53 extends Edge { } {
  src + dst = n53 + n61 }
one sig e54_55 extends Edge { } {
  src + dst = n54 + n55 }
one sig e55_54 extends Edge { } {
  src + dst = n54 + n55 }
one sig e54_62 extends Edge { } {
  src + dst = n54 + n62 }
one sig e62_54 extends Edge { } {
  src + dst = n54 + n62 }
one sig e55_63 extends Edge { } {
  src + dst = n55 + n63 }
one sig e63_55 extends Edge { } {
  src + dst = n55 + n63 }
one sig e56_57 extends Edge { } {
  src + dst = n56 + n57 }
one sig e57_56 extends Edge { } {
  src + dst = n56 + n57 }
one sig e56_58 extends Edge { } {
  src + dst = n56 + n58 }
one sig e58_56 extends Edge { } {
  src + dst = n56 + n58 }
one sig e56_60 extends Edge { } {
  src + dst = n56 + n60 }
one sig e60_56 extends Edge { } {
  src + dst = n56 + n60 }
one sig e57_59 extends Edge { } {
  src + dst = n57 + n59 }
one sig e59_57 extends Edge { } {
  src + dst = n57 + n59 }
one sig e57_61 extends Edge { } {
  src + dst = n57 + n61 }
one sig e61_57 extends Edge { } {
  src + dst = n57 + n61 }
one sig e58_59 extends Edge { } {
  src + dst = n58 + n59 }
one sig e59_58 extends Edge { } {
  src + dst = n58 + n59 }
one sig e58_62 extends Edge { } {
  src + dst = n58 + n62 }
one sig e62_58 extends Edge { } {
  src + dst = n58 + n62 }
one sig e59_63 extends Edge { } {
  src + dst = n59 + n63 }
one sig e63_59 extends Edge { } {
  src + dst = n59 + n63 }
one sig e60_61 extends Edge { } {
  src + dst = n60 + n61 }
one sig e61_60 extends Edge { } {
  src + dst = n60 + n61 }
one sig e60_62 extends Edge { } {
  src + dst = n60 + n62 }
one sig e62_60 extends Edge { } {
  src + dst = n60 + n62 }
one sig e61_63 extends Edge { } {
  src + dst = n61 + n63 }
one sig e63_61 extends Edge { } {
  src + dst = n61 + n63 }
one sig e62_63 extends Edge { } {
  src + dst = n62 + n63 }
one sig e63_62 extends Edge { } {
  src + dst = n62 + n63 }
one sig Q6 extends Graph { } {
  nodes = n0 + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10 + n11 + n12 + n13 + n14 + n15 + n16 + n17 + n18 + n19 + n20 + n21 + n22 + n23 + n24 + n25 + n26 + n27 + n28 + n29 + n30 + n31 + n32 + n33 + n34 + n35 + n36 + n37 + n38 + n39 + n40 + n41 + n42 + n43 + n44 + n45 + n46 + n47 + n48 + n49 + n50 + n51 + n52 + n53 + n54 + n55 + n56 + n57 + n58 + n59 + n60 + n61 + n62 + n63
  edges = e0_1 + e0_2 + e0_4 + e0_8 + e0_16 + e0_32 + e1_3 + e1_5 + e1_9 + e1_17 + e1_33 + e2_3 + e2_6 + e2_10 + e2_18 + e2_34 + e3_7 + e3_11 + e3_19 + e3_35 + e4_5 + e4_6 + e4_12 + e4_20 + e4_36 + e5_7 + e5_13 + e5_21 + e5_37 + e6_7 + e6_14 + e6_22 + e6_38 + e7_15 + e7_23 + e7_39 + e8_9 + e8_10 + e8_12 + e8_24 + e8_40 + e9_11 + e9_13 + e9_25 + e9_41 + e10_11 + e10_14 + e10_26 + e10_42 + e11_15 + e11_27 + e11_43 + e12_13 + e12_14 + e12_28 + e12_44 + e13_15 + e13_29 + e13_45 + e14_15 + e14_30 + e14_46 + e15_31 + e15_47 + e16_17 + e16_18 + e16_20 + e16_24 + e16_48 + e17_19 + e17_21 + e17_25 + e17_49 + e18_19 + e18_22 + e18_26 + e18_50 + e19_23 + e19_27 + e19_51 + e20_21 + e20_22 + e20_28 + e20_52 + e21_23 + e21_29 + e21_53 + e22_23 + e22_30 + e22_54 + e23_31 + e23_55 + e24_25 + e24_26 + e24_28 + e24_56 + e25_27 + e25_29 + e25_57 + e26_27 + e26_30 + e26_58 + e27_31 + e27_59 + e28_29 + e28_30 + e28_60 + e29_31 + e29_61 + e30_31 + e30_62 + e31_63 + e32_33 + e32_34 + e32_36 + e32_40 + e32_48 + e33_35 + e33_37 + e33_41 + e33_49 + e34_35 + e34_38 + e34_42 + e34_50 + e35_39 + e35_43 + e35_51 + e36_37 + e36_38 + e36_44 + e36_52 + e37_39 + e37_45 + e37_53 + e38_39 + e38_46 + e38_54 + e39_47 + e39_55 + e40_41 + e40_42 + e40_44 + e40_56 + e41_43 + e41_45 + e41_57 + e42_43 + e42_46 + e42_58 + e43_47 + e43_59 + e44_45 + e44_46 + e44_60 + e45_47 + e45_61 + e46_47 + e46_62 + e47_63 + e48_49 + e48_50 + e48_52 + e48_56 + e49_51 + e49_53 + e49_57 + e50_51 + e50_54 + e50_58 + e51_55 + e51_59 + e52_53 + e52_54 + e52_60 + e53_55 + e53_61 + e54_55 + e54_62 + e55_63 + e56_57 + e56_58 + e56_60 + e57_59 + e57_61 + e58_59 + e58_62 + e59_63 + e60_61 + e60_62 + e61_63 + e62_63 + e1_0 + e2_0 + e4_0 + e8_0 + e16_0 + e32_0 + e3_1 + e5_1 + e9_1 + e17_1 + e33_1 + e3_2 + e6_2 + e10_2 + e18_2 + e34_2 + e7_3 + e11_3 + e19_3 + e35_3 + e5_4 + e6_4 + e12_4 + e20_4 + e36_4 + e7_5 + e13_5 + e21_5 + e37_5 + e7_6 + e14_6 + e22_6 + e38_6 + e15_7 + e23_7 + e39_7 + e9_8 + e10_8 + e12_8 + e24_8 + e40_8 + e11_9 + e13_9 + e25_9 + e41_9 + e11_10 + e14_10 + e26_10 + e42_10 + e15_11 + e27_11 + e43_11 + e13_12 + e14_12 + e28_12 + e44_12 + e15_13 + e29_13 + e45_13 + e15_14 + e30_14 + e46_14 + e31_15 + e47_15 + e17_16 + e18_16 + e20_16 + e24_16 + e48_16 + e19_17 + e21_17 + e25_17 + e49_17 + e19_18 + e22_18 + e26_18 + e50_18 + e23_19 + e27_19 + e51_19 + e21_20 + e22_20 + e28_20 + e52_20 + e23_21 + e29_21 + e53_21 + e23_22 + e30_22 + e54_22 + e31_23 + e55_23 + e25_24 + e26_24 + e28_24 + e56_24 + e27_25 + e29_25 + e57_25 + e27_26 + e30_26 + e58_26 + e31_27 + e59_27 + e29_28 + e30_28 + e60_28 + e31_29 + e61_29 + e31_30 + e62_30 + e63_31 + e33_32 + e34_32 + e36_32 + e40_32 + e48_32 + e35_33 + e37_33 + e41_33 + e49_33 + e35_34 + e38_34 + e42_34 + e50_34 + e39_35 + e43_35 + e51_35 + e37_36 + e38_36 + e44_36 + e52_36 + e39_37 + e45_37 + e53_37 + e39_38 + e46_38 + e54_38 + e47_39 + e55_39 + e41_40 + e42_40 + e44_40 + e56_40 + e43_41 + e45_41 + e57_41 + e43_42 + e46_42 + e58_42 + e47_43 + e59_43 + e45_44 + e46_44 + e60_44 + e47_45 + e61_45 + e47_46 + e62_46 + e63_47 + e49_48 + e50_48 + e52_48 + e56_48 + e51_49 + e53_49 + e57_49 + e51_50 + e54_50 + e58_50 + e55_51 + e59_51 + e53_52 + e54_52 + e60_52 + e55_53 + e61_53 + e55_54 + e62_54 + e63_55 + e57_56 + e58_56 + e60_56 + e59_57 + e61_57 + e59_58 + e62_58 + e63_59 + e61_60 + e62_60 + e63_61 + e63_62
}
