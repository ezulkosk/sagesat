open q5
open edgeAntipodal
/* n1 -> n2 in conn[g] if n1 and n2 are neighbours in graph g */


//all edges must belong to some graph
fact{ 
  all e : Edge | e in Q5.edges
  all n: Node | no n.val 
}

// symmetric and irreflexive
pred edgeProps[x : Graph] {
  (all e : x.edges | some e2 : x.edges | e.src = e2.dst && e.dst = e2.src) and (no e : Edge | e.src = e.dst)
}


pred edge_antipodal[x: Graph] {
  (antipodal[x] && nonsimple[x] && edgeProps[x]) implies antipodal_connected[x]
}

check ea {
  edge_antipodal[x]
} for exactly 2 Graph, 32 Node, 160 Edge
