
open graph
open edgeAntipodal
pred antipodal[x : Graph]{
one (e0_1 + e30_31) & x.edges
one (e0_2 + e29_31) & x.edges
one (e0_4 + e27_31) & x.edges
one (e0_8 + e23_31) & x.edges
one (e0_16 + e15_31) & x.edges
one (e1_3 + e28_30) & x.edges
one (e1_5 + e26_30) & x.edges
one (e1_9 + e22_30) & x.edges
one (e1_17 + e14_30) & x.edges
one (e2_3 + e28_29) & x.edges
one (e2_6 + e25_29) & x.edges
one (e2_10 + e21_29) & x.edges
one (e2_18 + e13_29) & x.edges
one (e3_7 + e24_28) & x.edges
one (e3_11 + e20_28) & x.edges
one (e3_19 + e12_28) & x.edges
one (e4_5 + e26_27) & x.edges
one (e4_6 + e25_27) & x.edges
one (e4_12 + e19_27) & x.edges
one (e4_20 + e11_27) & x.edges
one (e5_7 + e24_26) & x.edges
one (e5_13 + e18_26) & x.edges
one (e5_21 + e10_26) & x.edges
one (e6_7 + e24_25) & x.edges
one (e6_14 + e17_25) & x.edges
one (e6_22 + e9_25) & x.edges
one (e7_15 + e16_24) & x.edges
one (e7_23 + e8_24) & x.edges
one (e8_9 + e22_23) & x.edges
one (e8_10 + e21_23) & x.edges
one (e8_12 + e19_23) & x.edges
one (e9_11 + e20_22) & x.edges
one (e9_13 + e18_22) & x.edges
one (e10_11 + e20_21) & x.edges
one (e10_14 + e17_21) & x.edges
one (e11_15 + e16_20) & x.edges
one (e12_13 + e18_19) & x.edges
one (e12_14 + e17_19) & x.edges
one (e13_15 + e16_18) & x.edges
one (e14_15 + e16_17) & x.edges
}

pred nonsimple[x: Graph]{
(e0_1 not in x.edges && e1_3 in x.edges && e2_3 not in x.edges && e0_2 in x.edges) || (e0_1 in x.edges && e1_3 not in x.edges && e2_3 in x.edges && e0_2 not in x.edges)||
(e4_5 not in x.edges && e5_7 in x.edges && e6_7 not in x.edges && e4_6 in x.edges) || (e4_5 in x.edges && e5_7 not in x.edges && e6_7 in x.edges && e4_6 not in x.edges)||
(e8_9 not in x.edges && e9_11 in x.edges && e10_11 not in x.edges && e8_10 in x.edges) || (e8_9 in x.edges && e9_11 not in x.edges && e10_11 in x.edges && e8_10 not in x.edges)||
(e12_13 not in x.edges && e13_15 in x.edges && e14_15 not in x.edges && e12_14 in x.edges) || (e12_13 in x.edges && e13_15 not in x.edges && e14_15 in x.edges && e12_14 not in x.edges)||
(e16_17 not in x.edges && e17_19 in x.edges && e18_19 not in x.edges && e16_18 in x.edges) || (e16_17 in x.edges && e17_19 not in x.edges && e18_19 in x.edges && e16_18 not in x.edges)||
(e20_21 not in x.edges && e21_23 in x.edges && e22_23 not in x.edges && e20_22 in x.edges) || (e20_21 in x.edges && e21_23 not in x.edges && e22_23 in x.edges && e20_22 not in x.edges)||
(e24_25 not in x.edges && e25_27 in x.edges && e26_27 not in x.edges && e24_26 in x.edges) || (e24_25 in x.edges && e25_27 not in x.edges && e26_27 in x.edges && e24_26 not in x.edges)||
(e28_29 not in x.edges && e29_31 in x.edges && e30_31 not in x.edges && e28_30 in x.edges) || (e28_29 in x.edges && e29_31 not in x.edges && e30_31 in x.edges && e28_30 not in x.edges)||
(e0_1 not in x.edges && e1_5 in x.edges && e4_5 not in x.edges && e0_4 in x.edges) || (e0_1 in x.edges && e1_5 not in x.edges && e4_5 in x.edges && e0_4 not in x.edges)||
(e2_3 not in x.edges && e3_7 in x.edges && e6_7 not in x.edges && e2_6 in x.edges) || (e2_3 in x.edges && e3_7 not in x.edges && e6_7 in x.edges && e2_6 not in x.edges)||
(e8_9 not in x.edges && e9_13 in x.edges && e12_13 not in x.edges && e8_12 in x.edges) || (e8_9 in x.edges && e9_13 not in x.edges && e12_13 in x.edges && e8_12 not in x.edges)||
(e10_11 not in x.edges && e11_15 in x.edges && e14_15 not in x.edges && e10_14 in x.edges) || (e10_11 in x.edges && e11_15 not in x.edges && e14_15 in x.edges && e10_14 not in x.edges)||
(e16_17 not in x.edges && e17_21 in x.edges && e20_21 not in x.edges && e16_20 in x.edges) || (e16_17 in x.edges && e17_21 not in x.edges && e20_21 in x.edges && e16_20 not in x.edges)||
(e18_19 not in x.edges && e19_23 in x.edges && e22_23 not in x.edges && e18_22 in x.edges) || (e18_19 in x.edges && e19_23 not in x.edges && e22_23 in x.edges && e18_22 not in x.edges)||
(e24_25 not in x.edges && e25_29 in x.edges && e28_29 not in x.edges && e24_28 in x.edges) || (e24_25 in x.edges && e25_29 not in x.edges && e28_29 in x.edges && e24_28 not in x.edges)||
(e26_27 not in x.edges && e27_31 in x.edges && e30_31 not in x.edges && e26_30 in x.edges) || (e26_27 in x.edges && e27_31 not in x.edges && e30_31 in x.edges && e26_30 not in x.edges)||
(e0_2 not in x.edges && e2_6 in x.edges && e4_6 not in x.edges && e0_4 in x.edges) || (e0_2 in x.edges && e2_6 not in x.edges && e4_6 in x.edges && e0_4 not in x.edges)||
(e1_3 not in x.edges && e3_7 in x.edges && e5_7 not in x.edges && e1_5 in x.edges) || (e1_3 in x.edges && e3_7 not in x.edges && e5_7 in x.edges && e1_5 not in x.edges)||
(e8_10 not in x.edges && e10_14 in x.edges && e12_14 not in x.edges && e8_12 in x.edges) || (e8_10 in x.edges && e10_14 not in x.edges && e12_14 in x.edges && e8_12 not in x.edges)||
(e9_11 not in x.edges && e11_15 in x.edges && e13_15 not in x.edges && e9_13 in x.edges) || (e9_11 in x.edges && e11_15 not in x.edges && e13_15 in x.edges && e9_13 not in x.edges)||
(e16_18 not in x.edges && e18_22 in x.edges && e20_22 not in x.edges && e16_20 in x.edges) || (e16_18 in x.edges && e18_22 not in x.edges && e20_22 in x.edges && e16_20 not in x.edges)||
(e17_19 not in x.edges && e19_23 in x.edges && e21_23 not in x.edges && e17_21 in x.edges) || (e17_19 in x.edges && e19_23 not in x.edges && e21_23 in x.edges && e17_21 not in x.edges)||
(e24_26 not in x.edges && e26_30 in x.edges && e28_30 not in x.edges && e24_28 in x.edges) || (e24_26 in x.edges && e26_30 not in x.edges && e28_30 in x.edges && e24_28 not in x.edges)||
(e25_27 not in x.edges && e27_31 in x.edges && e29_31 not in x.edges && e25_29 in x.edges) || (e25_27 in x.edges && e27_31 not in x.edges && e29_31 in x.edges && e25_29 not in x.edges)||
(e0_1 not in x.edges && e1_9 in x.edges && e8_9 not in x.edges && e0_8 in x.edges) || (e0_1 in x.edges && e1_9 not in x.edges && e8_9 in x.edges && e0_8 not in x.edges)||
(e2_3 not in x.edges && e3_11 in x.edges && e10_11 not in x.edges && e2_10 in x.edges) || (e2_3 in x.edges && e3_11 not in x.edges && e10_11 in x.edges && e2_10 not in x.edges)||
(e4_5 not in x.edges && e5_13 in x.edges && e12_13 not in x.edges && e4_12 in x.edges) || (e4_5 in x.edges && e5_13 not in x.edges && e12_13 in x.edges && e4_12 not in x.edges)||
(e6_7 not in x.edges && e7_15 in x.edges && e14_15 not in x.edges && e6_14 in x.edges) || (e6_7 in x.edges && e7_15 not in x.edges && e14_15 in x.edges && e6_14 not in x.edges)||
(e16_17 not in x.edges && e17_25 in x.edges && e24_25 not in x.edges && e16_24 in x.edges) || (e16_17 in x.edges && e17_25 not in x.edges && e24_25 in x.edges && e16_24 not in x.edges)||
(e18_19 not in x.edges && e19_27 in x.edges && e26_27 not in x.edges && e18_26 in x.edges) || (e18_19 in x.edges && e19_27 not in x.edges && e26_27 in x.edges && e18_26 not in x.edges)||
(e20_21 not in x.edges && e21_29 in x.edges && e28_29 not in x.edges && e20_28 in x.edges) || (e20_21 in x.edges && e21_29 not in x.edges && e28_29 in x.edges && e20_28 not in x.edges)||
(e22_23 not in x.edges && e23_31 in x.edges && e30_31 not in x.edges && e22_30 in x.edges) || (e22_23 in x.edges && e23_31 not in x.edges && e30_31 in x.edges && e22_30 not in x.edges)||
(e0_2 not in x.edges && e2_10 in x.edges && e8_10 not in x.edges && e0_8 in x.edges) || (e0_2 in x.edges && e2_10 not in x.edges && e8_10 in x.edges && e0_8 not in x.edges)||
(e1_3 not in x.edges && e3_11 in x.edges && e9_11 not in x.edges && e1_9 in x.edges) || (e1_3 in x.edges && e3_11 not in x.edges && e9_11 in x.edges && e1_9 not in x.edges)||
(e4_6 not in x.edges && e6_14 in x.edges && e12_14 not in x.edges && e4_12 in x.edges) || (e4_6 in x.edges && e6_14 not in x.edges && e12_14 in x.edges && e4_12 not in x.edges)||
(e5_7 not in x.edges && e7_15 in x.edges && e13_15 not in x.edges && e5_13 in x.edges) || (e5_7 in x.edges && e7_15 not in x.edges && e13_15 in x.edges && e5_13 not in x.edges)||
(e16_18 not in x.edges && e18_26 in x.edges && e24_26 not in x.edges && e16_24 in x.edges) || (e16_18 in x.edges && e18_26 not in x.edges && e24_26 in x.edges && e16_24 not in x.edges)||
(e17_19 not in x.edges && e19_27 in x.edges && e25_27 not in x.edges && e17_25 in x.edges) || (e17_19 in x.edges && e19_27 not in x.edges && e25_27 in x.edges && e17_25 not in x.edges)||
(e20_22 not in x.edges && e22_30 in x.edges && e28_30 not in x.edges && e20_28 in x.edges) || (e20_22 in x.edges && e22_30 not in x.edges && e28_30 in x.edges && e20_28 not in x.edges)||
(e21_23 not in x.edges && e23_31 in x.edges && e29_31 not in x.edges && e21_29 in x.edges) || (e21_23 in x.edges && e23_31 not in x.edges && e29_31 in x.edges && e21_29 not in x.edges)||
(e0_4 not in x.edges && e4_12 in x.edges && e8_12 not in x.edges && e0_8 in x.edges) || (e0_4 in x.edges && e4_12 not in x.edges && e8_12 in x.edges && e0_8 not in x.edges)||
(e1_5 not in x.edges && e5_13 in x.edges && e9_13 not in x.edges && e1_9 in x.edges) || (e1_5 in x.edges && e5_13 not in x.edges && e9_13 in x.edges && e1_9 not in x.edges)||
(e2_6 not in x.edges && e6_14 in x.edges && e10_14 not in x.edges && e2_10 in x.edges) || (e2_6 in x.edges && e6_14 not in x.edges && e10_14 in x.edges && e2_10 not in x.edges)||
(e3_7 not in x.edges && e7_15 in x.edges && e11_15 not in x.edges && e3_11 in x.edges) || (e3_7 in x.edges && e7_15 not in x.edges && e11_15 in x.edges && e3_11 not in x.edges)||
(e16_20 not in x.edges && e20_28 in x.edges && e24_28 not in x.edges && e16_24 in x.edges) || (e16_20 in x.edges && e20_28 not in x.edges && e24_28 in x.edges && e16_24 not in x.edges)||
(e17_21 not in x.edges && e21_29 in x.edges && e25_29 not in x.edges && e17_25 in x.edges) || (e17_21 in x.edges && e21_29 not in x.edges && e25_29 in x.edges && e17_25 not in x.edges)||
(e18_22 not in x.edges && e22_30 in x.edges && e26_30 not in x.edges && e18_26 in x.edges) || (e18_22 in x.edges && e22_30 not in x.edges && e26_30 in x.edges && e18_26 not in x.edges)||
(e19_23 not in x.edges && e23_31 in x.edges && e27_31 not in x.edges && e19_27 in x.edges) || (e19_23 in x.edges && e23_31 not in x.edges && e27_31 in x.edges && e19_27 not in x.edges)||
(e0_1 not in x.edges && e1_17 in x.edges && e16_17 not in x.edges && e0_16 in x.edges) || (e0_1 in x.edges && e1_17 not in x.edges && e16_17 in x.edges && e0_16 not in x.edges)||
(e2_3 not in x.edges && e3_19 in x.edges && e18_19 not in x.edges && e2_18 in x.edges) || (e2_3 in x.edges && e3_19 not in x.edges && e18_19 in x.edges && e2_18 not in x.edges)||
(e4_5 not in x.edges && e5_21 in x.edges && e20_21 not in x.edges && e4_20 in x.edges) || (e4_5 in x.edges && e5_21 not in x.edges && e20_21 in x.edges && e4_20 not in x.edges)||
(e6_7 not in x.edges && e7_23 in x.edges && e22_23 not in x.edges && e6_22 in x.edges) || (e6_7 in x.edges && e7_23 not in x.edges && e22_23 in x.edges && e6_22 not in x.edges)||
(e8_9 not in x.edges && e9_25 in x.edges && e24_25 not in x.edges && e8_24 in x.edges) || (e8_9 in x.edges && e9_25 not in x.edges && e24_25 in x.edges && e8_24 not in x.edges)||
(e10_11 not in x.edges && e11_27 in x.edges && e26_27 not in x.edges && e10_26 in x.edges) || (e10_11 in x.edges && e11_27 not in x.edges && e26_27 in x.edges && e10_26 not in x.edges)||
(e12_13 not in x.edges && e13_29 in x.edges && e28_29 not in x.edges && e12_28 in x.edges) || (e12_13 in x.edges && e13_29 not in x.edges && e28_29 in x.edges && e12_28 not in x.edges)||
(e14_15 not in x.edges && e15_31 in x.edges && e30_31 not in x.edges && e14_30 in x.edges) || (e14_15 in x.edges && e15_31 not in x.edges && e30_31 in x.edges && e14_30 not in x.edges)||
(e0_2 not in x.edges && e2_18 in x.edges && e16_18 not in x.edges && e0_16 in x.edges) || (e0_2 in x.edges && e2_18 not in x.edges && e16_18 in x.edges && e0_16 not in x.edges)||
(e1_3 not in x.edges && e3_19 in x.edges && e17_19 not in x.edges && e1_17 in x.edges) || (e1_3 in x.edges && e3_19 not in x.edges && e17_19 in x.edges && e1_17 not in x.edges)||
(e4_6 not in x.edges && e6_22 in x.edges && e20_22 not in x.edges && e4_20 in x.edges) || (e4_6 in x.edges && e6_22 not in x.edges && e20_22 in x.edges && e4_20 not in x.edges)||
(e5_7 not in x.edges && e7_23 in x.edges && e21_23 not in x.edges && e5_21 in x.edges) || (e5_7 in x.edges && e7_23 not in x.edges && e21_23 in x.edges && e5_21 not in x.edges)||
(e8_10 not in x.edges && e10_26 in x.edges && e24_26 not in x.edges && e8_24 in x.edges) || (e8_10 in x.edges && e10_26 not in x.edges && e24_26 in x.edges && e8_24 not in x.edges)||
(e9_11 not in x.edges && e11_27 in x.edges && e25_27 not in x.edges && e9_25 in x.edges) || (e9_11 in x.edges && e11_27 not in x.edges && e25_27 in x.edges && e9_25 not in x.edges)||
(e12_14 not in x.edges && e14_30 in x.edges && e28_30 not in x.edges && e12_28 in x.edges) || (e12_14 in x.edges && e14_30 not in x.edges && e28_30 in x.edges && e12_28 not in x.edges)||
(e13_15 not in x.edges && e15_31 in x.edges && e29_31 not in x.edges && e13_29 in x.edges) || (e13_15 in x.edges && e15_31 not in x.edges && e29_31 in x.edges && e13_29 not in x.edges)||
(e0_4 not in x.edges && e4_20 in x.edges && e16_20 not in x.edges && e0_16 in x.edges) || (e0_4 in x.edges && e4_20 not in x.edges && e16_20 in x.edges && e0_16 not in x.edges)||
(e1_5 not in x.edges && e5_21 in x.edges && e17_21 not in x.edges && e1_17 in x.edges) || (e1_5 in x.edges && e5_21 not in x.edges && e17_21 in x.edges && e1_17 not in x.edges)||
(e2_6 not in x.edges && e6_22 in x.edges && e18_22 not in x.edges && e2_18 in x.edges) || (e2_6 in x.edges && e6_22 not in x.edges && e18_22 in x.edges && e2_18 not in x.edges)||
(e3_7 not in x.edges && e7_23 in x.edges && e19_23 not in x.edges && e3_19 in x.edges) || (e3_7 in x.edges && e7_23 not in x.edges && e19_23 in x.edges && e3_19 not in x.edges)||
(e8_12 not in x.edges && e12_28 in x.edges && e24_28 not in x.edges && e8_24 in x.edges) || (e8_12 in x.edges && e12_28 not in x.edges && e24_28 in x.edges && e8_24 not in x.edges)||
(e9_13 not in x.edges && e13_29 in x.edges && e25_29 not in x.edges && e9_25 in x.edges) || (e9_13 in x.edges && e13_29 not in x.edges && e25_29 in x.edges && e9_25 not in x.edges)||
(e10_14 not in x.edges && e14_30 in x.edges && e26_30 not in x.edges && e10_26 in x.edges) || (e10_14 in x.edges && e14_30 not in x.edges && e26_30 in x.edges && e10_26 not in x.edges)||
(e11_15 not in x.edges && e15_31 in x.edges && e27_31 not in x.edges && e11_27 in x.edges) || (e11_15 in x.edges && e15_31 not in x.edges && e27_31 in x.edges && e11_27 not in x.edges)||
(e0_8 not in x.edges && e8_24 in x.edges && e16_24 not in x.edges && e0_16 in x.edges) || (e0_8 in x.edges && e8_24 not in x.edges && e16_24 in x.edges && e0_16 not in x.edges)||
(e1_9 not in x.edges && e9_25 in x.edges && e17_25 not in x.edges && e1_17 in x.edges) || (e1_9 in x.edges && e9_25 not in x.edges && e17_25 in x.edges && e1_17 not in x.edges)||
(e2_10 not in x.edges && e10_26 in x.edges && e18_26 not in x.edges && e2_18 in x.edges) || (e2_10 in x.edges && e10_26 not in x.edges && e18_26 in x.edges && e2_18 not in x.edges)||
(e3_11 not in x.edges && e11_27 in x.edges && e19_27 not in x.edges && e3_19 in x.edges) || (e3_11 in x.edges && e11_27 not in x.edges && e19_27 in x.edges && e3_19 not in x.edges)||
(e4_12 not in x.edges && e12_28 in x.edges && e20_28 not in x.edges && e4_20 in x.edges) || (e4_12 in x.edges && e12_28 not in x.edges && e20_28 in x.edges && e4_20 not in x.edges)||
(e5_13 not in x.edges && e13_29 in x.edges && e21_29 not in x.edges && e5_21 in x.edges) || (e5_13 in x.edges && e13_29 not in x.edges && e21_29 in x.edges && e5_21 not in x.edges)||
(e6_14 not in x.edges && e14_30 in x.edges && e22_30 not in x.edges && e6_22 in x.edges) || (e6_14 in x.edges && e14_30 not in x.edges && e22_30 in x.edges && e6_22 not in x.edges)||
(e7_15 not in x.edges && e15_31 in x.edges && e23_31 not in x.edges && e7_23 in x.edges) || (e7_15 in x.edges && e15_31 not in x.edges && e23_31 in x.edges && e7_23 not in x.edges)
}

pred antipodal_connected[x:Graph]{
connected[n0, n31, x] ||
connected[n1, n30, x] ||
connected[n2, n29, x] ||
connected[n3, n28, x] ||
connected[n4, n27, x] ||
connected[n5, n26, x] ||
connected[n6, n25, x] ||
connected[n7, n24, x] ||
connected[n8, n23, x] ||
connected[n9, n22, x] ||
connected[n10, n21, x] ||
connected[n11, n20, x] ||
connected[n12, n19, x] ||
connected[n13, n18, x] ||
connected[n14, n17, x] ||
connected[n15, n16, x]
}

one sig n0 extends Node { }
one sig n1 extends Node { }
one sig n2 extends Node { }
one sig n3 extends Node { }
one sig n4 extends Node { }
one sig n5 extends Node { }
one sig n6 extends Node { }
one sig n7 extends Node { }
one sig n8 extends Node { }
one sig n9 extends Node { }
one sig n10 extends Node { }
one sig n11 extends Node { }
one sig n12 extends Node { }
one sig n13 extends Node { }
one sig n14 extends Node { }
one sig n15 extends Node { }
one sig n16 extends Node { }
one sig n17 extends Node { }
one sig n18 extends Node { }
one sig n19 extends Node { }
one sig n20 extends Node { }
one sig n21 extends Node { }
one sig n22 extends Node { }
one sig n23 extends Node { }
one sig n24 extends Node { }
one sig n25 extends Node { }
one sig n26 extends Node { }
one sig n27 extends Node { }
one sig n28 extends Node { }
one sig n29 extends Node { }
one sig n30 extends Node { }
one sig n31 extends Node { }

one sig e0_1 extends Edge { } {
  src + dst = n0 + n1 }
one sig e1_0 extends Edge { } {
  src + dst = n0 + n1 }
one sig e0_2 extends Edge { } {
  src + dst = n0 + n2 }
one sig e2_0 extends Edge { } {
  src + dst = n0 + n2 }
one sig e0_4 extends Edge { } {
  src + dst = n0 + n4 }
one sig e4_0 extends Edge { } {
  src + dst = n0 + n4 }
one sig e0_8 extends Edge { } {
  src + dst = n0 + n8 }
one sig e8_0 extends Edge { } {
  src + dst = n0 + n8 }
one sig e0_16 extends Edge { } {
  src + dst = n0 + n16 }
one sig e16_0 extends Edge { } {
  src + dst = n0 + n16 }
one sig e1_3 extends Edge { } {
  src + dst = n1 + n3 }
one sig e3_1 extends Edge { } {
  src + dst = n1 + n3 }
one sig e1_5 extends Edge { } {
  src + dst = n1 + n5 }
one sig e5_1 extends Edge { } {
  src + dst = n1 + n5 }
one sig e1_9 extends Edge { } {
  src + dst = n1 + n9 }
one sig e9_1 extends Edge { } {
  src + dst = n1 + n9 }
one sig e1_17 extends Edge { } {
  src + dst = n1 + n17 }
one sig e17_1 extends Edge { } {
  src + dst = n1 + n17 }
one sig e2_3 extends Edge { } {
  src + dst = n2 + n3 }
one sig e3_2 extends Edge { } {
  src + dst = n2 + n3 }
one sig e2_6 extends Edge { } {
  src + dst = n2 + n6 }
one sig e6_2 extends Edge { } {
  src + dst = n2 + n6 }
one sig e2_10 extends Edge { } {
  src + dst = n2 + n10 }
one sig e10_2 extends Edge { } {
  src + dst = n2 + n10 }
one sig e2_18 extends Edge { } {
  src + dst = n2 + n18 }
one sig e18_2 extends Edge { } {
  src + dst = n2 + n18 }
one sig e3_7 extends Edge { } {
  src + dst = n3 + n7 }
one sig e7_3 extends Edge { } {
  src + dst = n3 + n7 }
one sig e3_11 extends Edge { } {
  src + dst = n3 + n11 }
one sig e11_3 extends Edge { } {
  src + dst = n3 + n11 }
one sig e3_19 extends Edge { } {
  src + dst = n3 + n19 }
one sig e19_3 extends Edge { } {
  src + dst = n3 + n19 }
one sig e4_5 extends Edge { } {
  src + dst = n4 + n5 }
one sig e5_4 extends Edge { } {
  src + dst = n4 + n5 }
one sig e4_6 extends Edge { } {
  src + dst = n4 + n6 }
one sig e6_4 extends Edge { } {
  src + dst = n4 + n6 }
one sig e4_12 extends Edge { } {
  src + dst = n4 + n12 }
one sig e12_4 extends Edge { } {
  src + dst = n4 + n12 }
one sig e4_20 extends Edge { } {
  src + dst = n4 + n20 }
one sig e20_4 extends Edge { } {
  src + dst = n4 + n20 }
one sig e5_7 extends Edge { } {
  src + dst = n5 + n7 }
one sig e7_5 extends Edge { } {
  src + dst = n5 + n7 }
one sig e5_13 extends Edge { } {
  src + dst = n5 + n13 }
one sig e13_5 extends Edge { } {
  src + dst = n5 + n13 }
one sig e5_21 extends Edge { } {
  src + dst = n5 + n21 }
one sig e21_5 extends Edge { } {
  src + dst = n5 + n21 }
one sig e6_7 extends Edge { } {
  src + dst = n6 + n7 }
one sig e7_6 extends Edge { } {
  src + dst = n6 + n7 }
one sig e6_14 extends Edge { } {
  src + dst = n6 + n14 }
one sig e14_6 extends Edge { } {
  src + dst = n6 + n14 }
one sig e6_22 extends Edge { } {
  src + dst = n6 + n22 }
one sig e22_6 extends Edge { } {
  src + dst = n6 + n22 }
one sig e7_15 extends Edge { } {
  src + dst = n7 + n15 }
one sig e15_7 extends Edge { } {
  src + dst = n7 + n15 }
one sig e7_23 extends Edge { } {
  src + dst = n7 + n23 }
one sig e23_7 extends Edge { } {
  src + dst = n7 + n23 }
one sig e8_9 extends Edge { } {
  src + dst = n8 + n9 }
one sig e9_8 extends Edge { } {
  src + dst = n8 + n9 }
one sig e8_10 extends Edge { } {
  src + dst = n8 + n10 }
one sig e10_8 extends Edge { } {
  src + dst = n8 + n10 }
one sig e8_12 extends Edge { } {
  src + dst = n8 + n12 }
one sig e12_8 extends Edge { } {
  src + dst = n8 + n12 }
one sig e8_24 extends Edge { } {
  src + dst = n8 + n24 }
one sig e24_8 extends Edge { } {
  src + dst = n8 + n24 }
one sig e9_11 extends Edge { } {
  src + dst = n9 + n11 }
one sig e11_9 extends Edge { } {
  src + dst = n9 + n11 }
one sig e9_13 extends Edge { } {
  src + dst = n9 + n13 }
one sig e13_9 extends Edge { } {
  src + dst = n9 + n13 }
one sig e9_25 extends Edge { } {
  src + dst = n9 + n25 }
one sig e25_9 extends Edge { } {
  src + dst = n9 + n25 }
one sig e10_11 extends Edge { } {
  src + dst = n10 + n11 }
one sig e11_10 extends Edge { } {
  src + dst = n10 + n11 }
one sig e10_14 extends Edge { } {
  src + dst = n10 + n14 }
one sig e14_10 extends Edge { } {
  src + dst = n10 + n14 }
one sig e10_26 extends Edge { } {
  src + dst = n10 + n26 }
one sig e26_10 extends Edge { } {
  src + dst = n10 + n26 }
one sig e11_15 extends Edge { } {
  src + dst = n11 + n15 }
one sig e15_11 extends Edge { } {
  src + dst = n11 + n15 }
one sig e11_27 extends Edge { } {
  src + dst = n11 + n27 }
one sig e27_11 extends Edge { } {
  src + dst = n11 + n27 }
one sig e12_13 extends Edge { } {
  src + dst = n12 + n13 }
one sig e13_12 extends Edge { } {
  src + dst = n12 + n13 }
one sig e12_14 extends Edge { } {
  src + dst = n12 + n14 }
one sig e14_12 extends Edge { } {
  src + dst = n12 + n14 }
one sig e12_28 extends Edge { } {
  src + dst = n12 + n28 }
one sig e28_12 extends Edge { } {
  src + dst = n12 + n28 }
one sig e13_15 extends Edge { } {
  src + dst = n13 + n15 }
one sig e15_13 extends Edge { } {
  src + dst = n13 + n15 }
one sig e13_29 extends Edge { } {
  src + dst = n13 + n29 }
one sig e29_13 extends Edge { } {
  src + dst = n13 + n29 }
one sig e14_15 extends Edge { } {
  src + dst = n14 + n15 }
one sig e15_14 extends Edge { } {
  src + dst = n14 + n15 }
one sig e14_30 extends Edge { } {
  src + dst = n14 + n30 }
one sig e30_14 extends Edge { } {
  src + dst = n14 + n30 }
one sig e15_31 extends Edge { } {
  src + dst = n15 + n31 }
one sig e31_15 extends Edge { } {
  src + dst = n15 + n31 }
one sig e16_17 extends Edge { } {
  src + dst = n16 + n17 }
one sig e17_16 extends Edge { } {
  src + dst = n16 + n17 }
one sig e16_18 extends Edge { } {
  src + dst = n16 + n18 }
one sig e18_16 extends Edge { } {
  src + dst = n16 + n18 }
one sig e16_20 extends Edge { } {
  src + dst = n16 + n20 }
one sig e20_16 extends Edge { } {
  src + dst = n16 + n20 }
one sig e16_24 extends Edge { } {
  src + dst = n16 + n24 }
one sig e24_16 extends Edge { } {
  src + dst = n16 + n24 }
one sig e17_19 extends Edge { } {
  src + dst = n17 + n19 }
one sig e19_17 extends Edge { } {
  src + dst = n17 + n19 }
one sig e17_21 extends Edge { } {
  src + dst = n17 + n21 }
one sig e21_17 extends Edge { } {
  src + dst = n17 + n21 }
one sig e17_25 extends Edge { } {
  src + dst = n17 + n25 }
one sig e25_17 extends Edge { } {
  src + dst = n17 + n25 }
one sig e18_19 extends Edge { } {
  src + dst = n18 + n19 }
one sig e19_18 extends Edge { } {
  src + dst = n18 + n19 }
one sig e18_22 extends Edge { } {
  src + dst = n18 + n22 }
one sig e22_18 extends Edge { } {
  src + dst = n18 + n22 }
one sig e18_26 extends Edge { } {
  src + dst = n18 + n26 }
one sig e26_18 extends Edge { } {
  src + dst = n18 + n26 }
one sig e19_23 extends Edge { } {
  src + dst = n19 + n23 }
one sig e23_19 extends Edge { } {
  src + dst = n19 + n23 }
one sig e19_27 extends Edge { } {
  src + dst = n19 + n27 }
one sig e27_19 extends Edge { } {
  src + dst = n19 + n27 }
one sig e20_21 extends Edge { } {
  src + dst = n20 + n21 }
one sig e21_20 extends Edge { } {
  src + dst = n20 + n21 }
one sig e20_22 extends Edge { } {
  src + dst = n20 + n22 }
one sig e22_20 extends Edge { } {
  src + dst = n20 + n22 }
one sig e20_28 extends Edge { } {
  src + dst = n20 + n28 }
one sig e28_20 extends Edge { } {
  src + dst = n20 + n28 }
one sig e21_23 extends Edge { } {
  src + dst = n21 + n23 }
one sig e23_21 extends Edge { } {
  src + dst = n21 + n23 }
one sig e21_29 extends Edge { } {
  src + dst = n21 + n29 }
one sig e29_21 extends Edge { } {
  src + dst = n21 + n29 }
one sig e22_23 extends Edge { } {
  src + dst = n22 + n23 }
one sig e23_22 extends Edge { } {
  src + dst = n22 + n23 }
one sig e22_30 extends Edge { } {
  src + dst = n22 + n30 }
one sig e30_22 extends Edge { } {
  src + dst = n22 + n30 }
one sig e23_31 extends Edge { } {
  src + dst = n23 + n31 }
one sig e31_23 extends Edge { } {
  src + dst = n23 + n31 }
one sig e24_25 extends Edge { } {
  src + dst = n24 + n25 }
one sig e25_24 extends Edge { } {
  src + dst = n24 + n25 }
one sig e24_26 extends Edge { } {
  src + dst = n24 + n26 }
one sig e26_24 extends Edge { } {
  src + dst = n24 + n26 }
one sig e24_28 extends Edge { } {
  src + dst = n24 + n28 }
one sig e28_24 extends Edge { } {
  src + dst = n24 + n28 }
one sig e25_27 extends Edge { } {
  src + dst = n25 + n27 }
one sig e27_25 extends Edge { } {
  src + dst = n25 + n27 }
one sig e25_29 extends Edge { } {
  src + dst = n25 + n29 }
one sig e29_25 extends Edge { } {
  src + dst = n25 + n29 }
one sig e26_27 extends Edge { } {
  src + dst = n26 + n27 }
one sig e27_26 extends Edge { } {
  src + dst = n26 + n27 }
one sig e26_30 extends Edge { } {
  src + dst = n26 + n30 }
one sig e30_26 extends Edge { } {
  src + dst = n26 + n30 }
one sig e27_31 extends Edge { } {
  src + dst = n27 + n31 }
one sig e31_27 extends Edge { } {
  src + dst = n27 + n31 }
one sig e28_29 extends Edge { } {
  src + dst = n28 + n29 }
one sig e29_28 extends Edge { } {
  src + dst = n28 + n29 }
one sig e28_30 extends Edge { } {
  src + dst = n28 + n30 }
one sig e30_28 extends Edge { } {
  src + dst = n28 + n30 }
one sig e29_31 extends Edge { } {
  src + dst = n29 + n31 }
one sig e31_29 extends Edge { } {
  src + dst = n29 + n31 }
one sig e30_31 extends Edge { } {
  src + dst = n30 + n31 }
one sig e31_30 extends Edge { } {
  src + dst = n30 + n31 }
one sig Q5 extends Graph { } {
  nodes = n0 + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10 + n11 + n12 + n13 + n14 + n15 + n16 + n17 + n18 + n19 + n20 + n21 + n22 + n23 + n24 + n25 + n26 + n27 + n28 + n29 + n30 + n31
  edges = e0_1 + e0_2 + e0_4 + e0_8 + e0_16 + e1_3 + e1_5 + e1_9 + e1_17 + e2_3 + e2_6 + e2_10 + e2_18 + e3_7 + e3_11 + e3_19 + e4_5 + e4_6 + e4_12 + e4_20 + e5_7 + e5_13 + e5_21 + e6_7 + e6_14 + e6_22 + e7_15 + e7_23 + e8_9 + e8_10 + e8_12 + e8_24 + e9_11 + e9_13 + e9_25 + e10_11 + e10_14 + e10_26 + e11_15 + e11_27 + e12_13 + e12_14 + e12_28 + e13_15 + e13_29 + e14_15 + e14_30 + e15_31 + e16_17 + e16_18 + e16_20 + e16_24 + e17_19 + e17_21 + e17_25 + e18_19 + e18_22 + e18_26 + e19_23 + e19_27 + e20_21 + e20_22 + e20_28 + e21_23 + e21_29 + e22_23 + e22_30 + e23_31 + e24_25 + e24_26 + e24_28 + e25_27 + e25_29 + e26_27 + e26_30 + e27_31 + e28_29 + e28_30 + e29_31 + e30_31 + e1_0 + e2_0 + e4_0 + e8_0 + e16_0 + e3_1 + e5_1 + e9_1 + e17_1 + e3_2 + e6_2 + e10_2 + e18_2 + e7_3 + e11_3 + e19_3 + e5_4 + e6_4 + e12_4 + e20_4 + e7_5 + e13_5 + e21_5 + e7_6 + e14_6 + e22_6 + e15_7 + e23_7 + e9_8 + e10_8 + e12_8 + e24_8 + e11_9 + e13_9 + e25_9 + e11_10 + e14_10 + e26_10 + e15_11 + e27_11 + e13_12 + e14_12 + e28_12 + e15_13 + e29_13 + e15_14 + e30_14 + e31_15 + e17_16 + e18_16 + e20_16 + e24_16 + e19_17 + e21_17 + e25_17 + e19_18 + e22_18 + e26_18 + e23_19 + e27_19 + e21_20 + e22_20 + e28_20 + e23_21 + e29_21 + e23_22 + e30_22 + e31_23 + e25_24 + e26_24 + e28_24 + e27_25 + e29_25 + e27_26 + e30_26 + e31_27 + e29_28 + e30_28 + e31_29 + e31_30
}
