
open graph
open edgeAntipodal
pred antipodal[x : Graph]{
one (e0_1 + e14_15) & x.edges
one (e0_2 + e13_15) & x.edges
one (e0_4 + e11_15) & x.edges
one (e0_8 + e7_15) & x.edges
one (e1_3 + e12_14) & x.edges
one (e1_5 + e10_14) & x.edges
one (e1_9 + e6_14) & x.edges
one (e2_3 + e12_13) & x.edges
one (e2_6 + e9_13) & x.edges
one (e2_10 + e5_13) & x.edges
one (e3_7 + e8_12) & x.edges
one (e3_11 + e4_12) & x.edges
one (e4_5 + e10_11) & x.edges
one (e4_6 + e9_11) & x.edges
one (e5_7 + e8_10) & x.edges
one (e6_7 + e8_9) & x.edges
}

pred nonsimple[x: Graph]{
(e0_1 not in x.edges && e1_3 in x.edges && e2_3 not in x.edges && e0_2 in x.edges) || (e0_1 in x.edges && e1_3 not in x.edges && e2_3 in x.edges && e0_2 not in x.edges)||
(e4_5 not in x.edges && e5_7 in x.edges && e6_7 not in x.edges && e4_6 in x.edges) || (e4_5 in x.edges && e5_7 not in x.edges && e6_7 in x.edges && e4_6 not in x.edges)||
(e8_9 not in x.edges && e9_11 in x.edges && e10_11 not in x.edges && e8_10 in x.edges) || (e8_9 in x.edges && e9_11 not in x.edges && e10_11 in x.edges && e8_10 not in x.edges)||
(e12_13 not in x.edges && e13_15 in x.edges && e14_15 not in x.edges && e12_14 in x.edges) || (e12_13 in x.edges && e13_15 not in x.edges && e14_15 in x.edges && e12_14 not in x.edges)||
(e0_1 not in x.edges && e1_5 in x.edges && e4_5 not in x.edges && e0_4 in x.edges) || (e0_1 in x.edges && e1_5 not in x.edges && e4_5 in x.edges && e0_4 not in x.edges)||
(e2_3 not in x.edges && e3_7 in x.edges && e6_7 not in x.edges && e2_6 in x.edges) || (e2_3 in x.edges && e3_7 not in x.edges && e6_7 in x.edges && e2_6 not in x.edges)||
(e8_9 not in x.edges && e9_13 in x.edges && e12_13 not in x.edges && e8_12 in x.edges) || (e8_9 in x.edges && e9_13 not in x.edges && e12_13 in x.edges && e8_12 not in x.edges)||
(e10_11 not in x.edges && e11_15 in x.edges && e14_15 not in x.edges && e10_14 in x.edges) || (e10_11 in x.edges && e11_15 not in x.edges && e14_15 in x.edges && e10_14 not in x.edges)||
(e0_2 not in x.edges && e2_6 in x.edges && e4_6 not in x.edges && e0_4 in x.edges) || (e0_2 in x.edges && e2_6 not in x.edges && e4_6 in x.edges && e0_4 not in x.edges)||
(e1_3 not in x.edges && e3_7 in x.edges && e5_7 not in x.edges && e1_5 in x.edges) || (e1_3 in x.edges && e3_7 not in x.edges && e5_7 in x.edges && e1_5 not in x.edges)||
(e8_10 not in x.edges && e10_14 in x.edges && e12_14 not in x.edges && e8_12 in x.edges) || (e8_10 in x.edges && e10_14 not in x.edges && e12_14 in x.edges && e8_12 not in x.edges)||
(e9_11 not in x.edges && e11_15 in x.edges && e13_15 not in x.edges && e9_13 in x.edges) || (e9_11 in x.edges && e11_15 not in x.edges && e13_15 in x.edges && e9_13 not in x.edges)||
(e0_1 not in x.edges && e1_9 in x.edges && e8_9 not in x.edges && e0_8 in x.edges) || (e0_1 in x.edges && e1_9 not in x.edges && e8_9 in x.edges && e0_8 not in x.edges)||
(e2_3 not in x.edges && e3_11 in x.edges && e10_11 not in x.edges && e2_10 in x.edges) || (e2_3 in x.edges && e3_11 not in x.edges && e10_11 in x.edges && e2_10 not in x.edges)||
(e4_5 not in x.edges && e5_13 in x.edges && e12_13 not in x.edges && e4_12 in x.edges) || (e4_5 in x.edges && e5_13 not in x.edges && e12_13 in x.edges && e4_12 not in x.edges)||
(e6_7 not in x.edges && e7_15 in x.edges && e14_15 not in x.edges && e6_14 in x.edges) || (e6_7 in x.edges && e7_15 not in x.edges && e14_15 in x.edges && e6_14 not in x.edges)||
(e0_2 not in x.edges && e2_10 in x.edges && e8_10 not in x.edges && e0_8 in x.edges) || (e0_2 in x.edges && e2_10 not in x.edges && e8_10 in x.edges && e0_8 not in x.edges)||
(e1_3 not in x.edges && e3_11 in x.edges && e9_11 not in x.edges && e1_9 in x.edges) || (e1_3 in x.edges && e3_11 not in x.edges && e9_11 in x.edges && e1_9 not in x.edges)||
(e4_6 not in x.edges && e6_14 in x.edges && e12_14 not in x.edges && e4_12 in x.edges) || (e4_6 in x.edges && e6_14 not in x.edges && e12_14 in x.edges && e4_12 not in x.edges)||
(e5_7 not in x.edges && e7_15 in x.edges && e13_15 not in x.edges && e5_13 in x.edges) || (e5_7 in x.edges && e7_15 not in x.edges && e13_15 in x.edges && e5_13 not in x.edges)||
(e0_4 not in x.edges && e4_12 in x.edges && e8_12 not in x.edges && e0_8 in x.edges) || (e0_4 in x.edges && e4_12 not in x.edges && e8_12 in x.edges && e0_8 not in x.edges)||
(e1_5 not in x.edges && e5_13 in x.edges && e9_13 not in x.edges && e1_9 in x.edges) || (e1_5 in x.edges && e5_13 not in x.edges && e9_13 in x.edges && e1_9 not in x.edges)||
(e2_6 not in x.edges && e6_14 in x.edges && e10_14 not in x.edges && e2_10 in x.edges) || (e2_6 in x.edges && e6_14 not in x.edges && e10_14 in x.edges && e2_10 not in x.edges)||
(e3_7 not in x.edges && e7_15 in x.edges && e11_15 not in x.edges && e3_11 in x.edges) || (e3_7 in x.edges && e7_15 not in x.edges && e11_15 in x.edges && e3_11 not in x.edges)
}

pred antipodal_connected[x:Graph]{
connected[n0, n15, x] ||
connected[n1, n14, x] ||
connected[n2, n13, x] ||
connected[n3, n12, x] ||
connected[n4, n11, x] ||
connected[n5, n10, x] ||
connected[n6, n9, x] ||
connected[n7, n8, x]
}

one sig n0 extends Node { }
one sig n1 extends Node { }
one sig n2 extends Node { }
one sig n3 extends Node { }
one sig n4 extends Node { }
one sig n5 extends Node { }
one sig n6 extends Node { }
one sig n7 extends Node { }
one sig n8 extends Node { }
one sig n9 extends Node { }
one sig n10 extends Node { }
one sig n11 extends Node { }
one sig n12 extends Node { }
one sig n13 extends Node { }
one sig n14 extends Node { }
one sig n15 extends Node { }

one sig e0_1 extends Edge { } {
  src + dst = n0 + n1 }
one sig e1_0 extends Edge { } {
  src + dst = n0 + n1 }
one sig e0_2 extends Edge { } {
  src + dst = n0 + n2 }
one sig e2_0 extends Edge { } {
  src + dst = n0 + n2 }
one sig e0_4 extends Edge { } {
  src + dst = n0 + n4 }
one sig e4_0 extends Edge { } {
  src + dst = n0 + n4 }
one sig e0_8 extends Edge { } {
  src + dst = n0 + n8 }
one sig e8_0 extends Edge { } {
  src + dst = n0 + n8 }
one sig e1_3 extends Edge { } {
  src + dst = n1 + n3 }
one sig e3_1 extends Edge { } {
  src + dst = n1 + n3 }
one sig e1_5 extends Edge { } {
  src + dst = n1 + n5 }
one sig e5_1 extends Edge { } {
  src + dst = n1 + n5 }
one sig e1_9 extends Edge { } {
  src + dst = n1 + n9 }
one sig e9_1 extends Edge { } {
  src + dst = n1 + n9 }
one sig e2_3 extends Edge { } {
  src + dst = n2 + n3 }
one sig e3_2 extends Edge { } {
  src + dst = n2 + n3 }
one sig e2_6 extends Edge { } {
  src + dst = n2 + n6 }
one sig e6_2 extends Edge { } {
  src + dst = n2 + n6 }
one sig e2_10 extends Edge { } {
  src + dst = n2 + n10 }
one sig e10_2 extends Edge { } {
  src + dst = n2 + n10 }
one sig e3_7 extends Edge { } {
  src + dst = n3 + n7 }
one sig e7_3 extends Edge { } {
  src + dst = n3 + n7 }
one sig e3_11 extends Edge { } {
  src + dst = n3 + n11 }
one sig e11_3 extends Edge { } {
  src + dst = n3 + n11 }
one sig e4_5 extends Edge { } {
  src + dst = n4 + n5 }
one sig e5_4 extends Edge { } {
  src + dst = n4 + n5 }
one sig e4_6 extends Edge { } {
  src + dst = n4 + n6 }
one sig e6_4 extends Edge { } {
  src + dst = n4 + n6 }
one sig e4_12 extends Edge { } {
  src + dst = n4 + n12 }
one sig e12_4 extends Edge { } {
  src + dst = n4 + n12 }
one sig e5_7 extends Edge { } {
  src + dst = n5 + n7 }
one sig e7_5 extends Edge { } {
  src + dst = n5 + n7 }
one sig e5_13 extends Edge { } {
  src + dst = n5 + n13 }
one sig e13_5 extends Edge { } {
  src + dst = n5 + n13 }
one sig e6_7 extends Edge { } {
  src + dst = n6 + n7 }
one sig e7_6 extends Edge { } {
  src + dst = n6 + n7 }
one sig e6_14 extends Edge { } {
  src + dst = n6 + n14 }
one sig e14_6 extends Edge { } {
  src + dst = n6 + n14 }
one sig e7_15 extends Edge { } {
  src + dst = n7 + n15 }
one sig e15_7 extends Edge { } {
  src + dst = n7 + n15 }
one sig e8_9 extends Edge { } {
  src + dst = n8 + n9 }
one sig e9_8 extends Edge { } {
  src + dst = n8 + n9 }
one sig e8_10 extends Edge { } {
  src + dst = n8 + n10 }
one sig e10_8 extends Edge { } {
  src + dst = n8 + n10 }
one sig e8_12 extends Edge { } {
  src + dst = n8 + n12 }
one sig e12_8 extends Edge { } {
  src + dst = n8 + n12 }
one sig e9_11 extends Edge { } {
  src + dst = n9 + n11 }
one sig e11_9 extends Edge { } {
  src + dst = n9 + n11 }
one sig e9_13 extends Edge { } {
  src + dst = n9 + n13 }
one sig e13_9 extends Edge { } {
  src + dst = n9 + n13 }
one sig e10_11 extends Edge { } {
  src + dst = n10 + n11 }
one sig e11_10 extends Edge { } {
  src + dst = n10 + n11 }
one sig e10_14 extends Edge { } {
  src + dst = n10 + n14 }
one sig e14_10 extends Edge { } {
  src + dst = n10 + n14 }
one sig e11_15 extends Edge { } {
  src + dst = n11 + n15 }
one sig e15_11 extends Edge { } {
  src + dst = n11 + n15 }
one sig e12_13 extends Edge { } {
  src + dst = n12 + n13 }
one sig e13_12 extends Edge { } {
  src + dst = n12 + n13 }
one sig e12_14 extends Edge { } {
  src + dst = n12 + n14 }
one sig e14_12 extends Edge { } {
  src + dst = n12 + n14 }
one sig e13_15 extends Edge { } {
  src + dst = n13 + n15 }
one sig e15_13 extends Edge { } {
  src + dst = n13 + n15 }
one sig e14_15 extends Edge { } {
  src + dst = n14 + n15 }
one sig e15_14 extends Edge { } {
  src + dst = n14 + n15 }
one sig Q4 extends Graph { } {
  nodes = n0 + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10 + n11 + n12 + n13 + n14 + n15
  edges = e0_1 + e0_2 + e0_4 + e0_8 + e1_3 + e1_5 + e1_9 + e2_3 + e2_6 + e2_10 + e3_7 + e3_11 + e4_5 + e4_6 + e4_12 + e5_7 + e5_13 + e6_7 + e6_14 + e7_15 + e8_9 + e8_10 + e8_12 + e9_11 + e9_13 + e10_11 + e10_14 + e11_15 + e12_13 + e12_14 + e13_15 + e14_15 + e1_0 + e2_0 + e4_0 + e8_0 + e3_1 + e5_1 + e9_1 + e3_2 + e6_2 + e10_2 + e7_3 + e11_3 + e5_4 + e6_4 + e12_4 + e7_5 + e13_5 + e7_6 + e14_6 + e15_7 + e9_8 + e10_8 + e12_8 + e11_9 + e13_9 + e11_10 + e14_10 + e15_11 + e13_12 + e14_12 + e15_13 + e15_14
}
