open graph

/* nodes connected by a given edge */
fun eNodes[e: Edge]: set Node { e.(src+dst) }

fun conn[g: Graph]: Node -> Node {
  {n1, n2: g.nodes | some e: g.edges | eNodes[e] = n1 + n2}
}

/* holds over (n1, n2, g) iff there's a path from n1 to n2 in graph g*/
pred connected[n1, n2: Node, g: Graph] {
  n1 -> n2 in ^(conn[g])
}

one sig x extends Graph {}
{
  //no extra nodes outside the matching
  nodes in edges.(src+dst) 
}
