import sys

import itertools

from hamilton_clean import parse_certificate, create_cube, hamming_distance


def check_clause(c, b2g, cas_var, cube_vertices, cube_edges):
    # ensure the cas predicate abstraction is in the clause
    assert cas_var in c

    #ensure the remaining variables correspond to edges
    graph_components = [b2g[i] for i in c if i != cas_var]

    assert set(graph_components).issubset(cube_edges)

    complemented_edges = list(set(cube_edges).difference(graph_components))

    # check if the complemented_edges form a hamiltonian cycle
    visited = []
    (first_vertex, next) = complemented_edges[0]
    complemented_edges.pop(0)
    visited.append(first_vertex)
    while True:
        next_edges = [(v1, v2) for (v1, v2) in complemented_edges if v1 == next or v2 == next]
        if len(next_edges) > 1:
            sys.exit("Bug in cycle detection")
        if len(next_edges) != 1:
            assert len(complemented_edges) == 0
            if next == first_vertex and set(visited) == set(cube_vertices):
                break
            else:
                sys.exit("Bug in cycle detection")
        visited.append(next)
        (v1, v2) = next_edges[0]
        complemented_edges.remove((v1, v2))
        if v1 == next:
            next = v2
        else:
            next = v1


def main(dims, cert_file, cnf_file):
    # create the hypercube, which is effectively a tuple containing a list of vertices and edges, represented as ints
    cube_vertices, cube_edges = create_cube(dims)

    # maps each graph component to its corresponding dimacs variable (g2b),
    # as well as the inverse map (b2g), according to the given certificate.
    # obtains the dimacs for the cas_var, as well as the learnt clauses
    g2b, b2g, cas_var, learnt_clauses = parse_certificate(cert_file)

    # Generate clauses -- throughout, we use the terms 'on' and 'off' to mean that the graph component
    #                     is in the model or not, respectively.
    clauses = []

    # assert the edge-implies-vertices constraints
    for e in cube_edges:
        # either the edge e is off, or both of its vertices v1, v2 are on
        # (!e or v1) and (!e or v2)
        (v1, v2) = e
        clauses.append([-g2b[e], g2b[v1]])
        clauses.append([-g2b[e], g2b[v2]])

    # assert the antipodal edge constraints
    # for every pair of antipodal edges e1, e2, we have e1 is on xor e2 is on
    for i in range(len(cube_edges) - 1):
        for j in range(i + 1, len(cube_edges)):
            e1 = cube_edges[i]
            e2 = cube_edges[j]
            b1u = ('{0:0' + str(dims) + 'b}').format(e1[0])
            b1v = ('{0:0' + str(dims) + 'b}').format(e1[1])
            b2u = ('{0:0' + str(dims) + 'b}').format(e2[0])
            b2v = ('{0:0' + str(dims) + 'b}').format(e2[1])
        # if the edges are antipodal, create the xor constraint
        if (hamming_distance(b1u, b2u) == dims and hamming_distance(b1v, b2v) == dims) or \
                    (hamming_distance(b1u, b2v) == dims and hamming_distance(b1v, b2u) == dims):
                clauses.append([-g2b[e1], g2b[e2]])
                clauses.append([g2b[e1], -g2b[e2]])

    # append non-simple labeling clauses
    # get faces
    combs = itertools.combinations(range(dims), dims - 2)
    prods = list(itertools.product([0, 1], repeat=(dims - 2)))
    face = list(itertools.product([0, 1], repeat=2))
    face_list = []
    for c in combs:
        for p in prods:
            s = []
            plist = list(p)
            xtaken = False
            for i in range(dims):
                if i in c:
                    s.append(plist.pop(0))
                else:
                    if xtaken:
                        s.append('y')
                    else:
                        s.append('x')
                        xtaken = True
            face_list.append(s)
    corners_list = []
    for f in face_list:
        face = list(itertools.product([0, 1], repeat=2))
        corners = list(itertools.repeat(f, 4))
        for i in range(len(face)):
            (x, y) = face[i]
            corners[i] = [x if j == 'x' else y if j == 'y' else j for j in corners[i]]
        corners = ["".join([str(k) for k in j]) for j in corners]
        corners = [int(j, 2) for j in corners]
        corners = [(min(corners[i], corners[j]), max(corners[i], corners[j])) for (i, j) in
                   [(0, 1), (1, 3), (3, 2), (2, 0)]]

        corners_list.append(corners)
    exprs = []
    for c in corners_list:
        print(c)
        sys.exit("TODO cnf this")
        block1 = Op(ID('and'), [solver.off(g, c[0]), solver.on(g, c[1]), solver.off(g, c[2]), solver.on(g, c[3])])
        block2 = Op(ID('and'), [solver.on(g, c[0]), solver.off(g, c[1]), solver.on(g, c[2]), solver.off(g, c[3])])
        exprs.append(Op(ID('or'), [block1, block2]))
    return Op(ID('or'), exprs)

    # append the negation of the cas_var literal, as in the paper (we want to ensure the predicate does not hold)
    clauses.append([-cas_var])

    sys.exit("Need to prune in mathcheck")
    # append learnt clauses from certificate
    for c in learnt_clauses:
        # ensure that the clause corresponds to equation 7 in the paper
        check_clause(c, b2g, cas_var, cube_vertices, cube_edges)
        clauses.append(c)

    # write header to cnf file
    out = open(cnf_file, 'w')
    out.write("p cnf " + str(len(cube_vertices) + len(cube_edges)) + " " + str(len(clauses)) + "\n")

    # write clauses
    for c in clauses:
        out.write(" ".join([str(i) for i in c]) + " 0\n")

    out.close()


if __name__ == '__main__':
    if len(sys.argv) != 4:
        sys.exit("Usage: python3 antipodal_clean #dims certificate_file out_cnf_file")
    main(int(sys.argv[1]), sys.argv[2], sys.argv[3])
